#!/usr/bin/bash

for dir in app/src/main/res/values-*
do
	LANG=$(echo $dir | sed "s/.*-//")

	# translate LANG to java Locale specifier
	case $LANG in
		"rBR")
			LANG='"pt", "br"'
			;;
		"rNO")
			LANG='"nb"'
			;;
		"rCN")
			LANG='"zh", "CN"'
			;;
		*)
			LANG="\"$LANG\""
			;;
	esac

	# echo $LANG
	{
		git log --format="%an" --follow $dir/strings.xml
	} | sort | uniq | sed "/Weblate\|Fynn Godau\|fynngodau/d" | xargs -i echo "new Translator(\"{}\", null, new Locale($LANG)),"
done

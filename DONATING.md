## Donations

To support the project, you may decide to make a monetary donation.

Donations reach me, Fynn Godau, and support my work. I
offer multiple way of donating. Donations are not tax-deductible.

### Wire transfer with SEPA

If you live within the SEPA space, this is the preferred way of donating as no fees are
deducted from the amount that I receive.

You may transfer funds to:

    Recipient            Fynn Godau
    IBAN                 DE37 4306 0967 1010 2633 00
    BIC                  GENODEM1GLS
    Reason for transfer  dawdle donation

### Credit card via Stripe

Due to [the fees that Stripe takes from your donation](https://stripe.com/en-de/pricing),
it is preferable if you donate via wire transfer if you can.

* [Donate in USD](https://donate.stripe.com/4gw2bF680go60tWeUY)
* [Donate in EUR](https://donate.stripe.com/8wM03x0NG5Js4KceUX)
* If you need to donate in another currency, [contact me](mailto:fynngodau@mailbox.org),
  and I will provide you with a link.


### Recurring donations via Liberapay

On Liberapay, you can set up a recurring donation with credit card or with SEPA direct debit in any currency of your choice.

For credit card, a fee according to the [Stripe fees](https://stripe.com/en-de/pricing) is
deducted before the amount reaches me. For SEPA direct debit, a 35 Cent fee is deducted.

[Head to dawdle's Liberapay profile](https://liberapay.com/dawdle/) to set up your donation.

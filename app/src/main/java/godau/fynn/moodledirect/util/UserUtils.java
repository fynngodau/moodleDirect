package godau.fynn.moodledirect.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;
import godau.fynn.moodledirect.activity.login.LoginActivity;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.data.persistence.UserAccount;
import godau.fynn.moodledirect.network.APIClient;

/**
 * Created by Harshit Agarwal on 24-11-2017.
 */

public class UserUtils {
    public static void logout(Context context) {
        new PreferenceHelper(context).logout();

        APIClient.clearMoodleInstance();
    }

    public static void logoutAndFinishAffinity(Context context) {

        logout(context);
        if (context instanceof Activity) {
            // Fill in moodle instance URL in login screen
            Intent intent = new Intent(context, LoginActivity.class);
            intent.setData(Uri.parse("moodlemobile://" + Constants.API_URL));

            ((Activity) context).finishAffinity();
            context.startActivity(intent);
        }
    }
}

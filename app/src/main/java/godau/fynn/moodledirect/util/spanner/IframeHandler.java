package godau.fynn.moodledirect.util.spanner;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.text.style.URLSpan;
import com.sysdata.htmlspanner.SpanStack;
import com.sysdata.htmlspanner.TagNodeHandler;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.util.Util;
import org.htmlcleaner.TagNode;

public class IframeHandler extends TagNodeHandler {

    private final Context context;

    public IframeHandler(Context context) {
        this.context = context;
    }

    @Override
    public void handleTagNode(TagNode node, SpannableStringBuilder builder, int start, int end, SpanStack spanStack) {
        String src = node.getAttributeByName("src");
        String title = null;

        if (node.hasAttribute("title")) {
            title = node.getAttributeByName("title");
            if (title.isEmpty()) title = null;
        }

        if (title == null) title = src;

        builder.append("\uFFFC")
                .append(" ")
                .append(title);

        Drawable drawable = loadDrawable();
        ImageSpan imageSpan = new ImageSpan(drawable);
        URLSpan urlSpan = new URLSpan(src);
        URLSpan urlSpan1 = new URLSpan(src);

        spanStack.pushSpan(imageSpan, start, start + 1);
        spanStack.pushSpan(urlSpan, start, start + 1);
        spanStack.pushSpan(urlSpan1, start + 2, builder.length());
    }

    @Override
    public boolean rendersContent() {
        return true;
    }

    public Drawable loadDrawable() {
        Drawable drawable = context.getDrawable(R.drawable.ic_open_externally);
        int size = Util.spToPx(16, context);
        drawable.setBounds(0, 0, size, size);
        return drawable;
    }
}

package godau.fynn.moodledirect.util;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.browser.customtabs.CustomTabsIntent;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.data.persistence.UserAccount;
import godau.fynn.moodledirect.network.NetworkStateReceiver;

/**
 * Provides UI components for autologin
 */
public class AutoLoginHelper {

    public static void openWithAutoLogin(Context context, View drawContext, @NonNull String url) {
        CustomTabsIntent intent = new CustomTabsIntent.Builder()
                .build();

        PreferenceHelper preferences = new PreferenceHelper(context);
        UserAccount userAccount = preferences.getUserAccount();

        if (userAccount.hasPrivateToken()
                && userAccount.getLastAutoLoginTime() + userAccount.getAutoLoginCooldown() < System.currentTimeMillis()
                && preferences.isAutoLoginEnabled()
                && !NetworkStateReceiver.getOfflineStatus()
        ) {

            Snackbar snackbar = Snackbar.make(
                    drawContext,
                    R.string.autologin_in_progress,
                    BaseTransientBottomBar.LENGTH_INDEFINITE);
            snackbar.show();

            userAccount.recordAutoLogin();

            ExceptionHandler.tryAndThenThread(
                    () -> MyApplication.moodle().getDispatch().getTool().getAutoLoginUrl(url),
                    autoUrl -> {
                        snackbar.dismiss();
                        intent.launchUrl(context, Uri.parse(autoUrl));
                    },
                    otherwise -> {
                        snackbar.dismiss();
                        intent.launchUrl(context, Uri.parse(url));
                    },
                    context
            );


        } else {
            Log.d(AutoLoginHelper.class.getSimpleName(),
                    "Autlogin conditions not fulfilled. Has " + (userAccount.hasPrivateToken()? "" : "no ") + "private token, " +
                            "cooldown ends in " + (userAccount.getLastAutoLoginTime() + userAccount.getAutoLoginCooldown() - System.currentTimeMillis()) + " millis, " +
                            "autologin is " + (preferences.isAutoLoginEnabled()? "" : "not ") + "enabled and " +
                            "offline mode is " + (NetworkStateReceiver.getOfflineStatus()? "enabled." : "disabled.")
            );
            // Do not perform autologin
            intent.launchUrl(context, Uri.parse(url));
        }
    }
}

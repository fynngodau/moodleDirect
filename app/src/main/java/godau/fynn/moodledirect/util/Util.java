package godau.fynn.moodledirect.util;

import android.content.Context;
import android.util.TypedValue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Set of utility functions that can be used throughout the entire project.
 *
 * @author abhijeetviswa
 * @since 21/04/2019
 */
public class Util {

    /**
     * Constructs a DateTime string in the local timezone
     *
     * @param epoch Unix epoch of the instant, in seconds
     * @return DateTime string in the format 10:10:10 AM 18-Nov-2019
     */
    @Deprecated
    public static String epochToDateTime(long epoch) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:m:ss a dd-MMM-yy");
        sdf.setTimeZone(TimeZone.getDefault());

        return sdf.format(new Date(epoch * 1000));
    }

    public static int dpToPx(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static int spToPx(float sp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }
}

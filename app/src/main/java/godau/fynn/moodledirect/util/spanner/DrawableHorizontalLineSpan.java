package godau.fynn.moodledirect.util.spanner;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.sysdata.htmlspanner.HtmlSpanner;
import com.sysdata.htmlspanner.style.Style;
import com.sysdata.htmlspanner.style.StyleValue;

public class DrawableHorizontalLineSpan extends com.sysdata.htmlspanner.spans.HorizontalLineSpan {

    private final Style style;
    private final Drawable drawable;

    public DrawableHorizontalLineSpan(Style style, Drawable drawable) {
        super(style);
        this.style = style;
        this.drawable = drawable;
    }

    @Override
    public void drawBackground(Canvas c, Paint p,
                               int left, int right,
                               int top, int baseline, int bottom,
                               CharSequence text, int start, int end,
                               int lnum) {
        int baseMargin = 0;

        if (style.getMarginLeft() != null) {
            StyleValue styleValue = style.getMarginLeft();

            if (styleValue.getUnit() == StyleValue.Unit.PX) {
                if (styleValue.getIntValue() > 0) {
                    baseMargin = styleValue.getIntValue();
                }
            } else if (styleValue.getFloatValue() > 0f) {
                baseMargin = (int) (styleValue.getFloatValue() * HtmlSpanner.HORIZONTAL_EM_WIDTH);
            }

            //Leave a little bit of room
            baseMargin--;
        }

        if (baseMargin > 0) {
            left = left + baseMargin;
        }

        int strokeWidth;

        if (style.getBorderWidth() != null && style.getBorderWidth().getUnit() == StyleValue.Unit.PX) {
            strokeWidth = style.getBorderWidth().getIntValue();
        } else {
            strokeWidth = drawable.getIntrinsicHeight();
        }

        Log.d("HorizontalSpan", "Drawing line with drawable");

        int center = (bottom + top) / 2;
        drawable.setBounds(left, center, right, center + strokeWidth);
        drawable.draw(c);
    }
}

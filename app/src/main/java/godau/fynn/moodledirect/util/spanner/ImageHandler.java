package godau.fynn.moodledirect.util.spanner;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import com.sysdata.htmlspanner.SpanStack;
import com.sysdata.htmlspanner.TagNodeHandler;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.util.Util;
import org.htmlcleaner.TagNode;

class ImageHandler extends TagNodeHandler {

    private final Context context;

    public ImageHandler(Context context) {
        this.context = context;
    }

    @Override
    public void handleTagNode(TagNode node, SpannableStringBuilder builder,
                              int start, int end, SpanStack spanStack) {
        String src = node.getAttributeByName("src");


        builder.append("\uFFFC");

        Drawable drawable = loadDrawable(src);
        ImageSpan imageSpan;

        if (node.hasAttribute("width") && node.hasAttribute("height")) {
            imageSpan  = new EnhancedImageSpan(drawable, src);
            ((EnhancedImageSpan) imageSpan).setWidth(Util.dpToPx(Float.parseFloat(node.getAttributeByName("width")), context));
            ((EnhancedImageSpan) imageSpan).setHeight(Util.dpToPx(Float.parseFloat(node.getAttributeByName("height")), context));
        } else {
            imageSpan = new ImageSpan(drawable, src);
        }

        spanStack.pushSpan(imageSpan, start, builder.length());
    }

    @Override
    public boolean rendersContent() {
        return true;
    }

    public Drawable loadDrawable(String source) {
        Drawable drawable = context.getDrawable(R.drawable.ic_placeholder);
        int size = Util.spToPx(16, context);
        drawable.setBounds(0, 0, size, size);
        return drawable;
    }
}

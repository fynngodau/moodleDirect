package godau.fynn.moodledirect.util.spanner;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.util.Log;

import com.sysdata.htmlspanner.SpanStack;
import com.sysdata.htmlspanner.handlers.StyledTextHandler;
import com.sysdata.htmlspanner.style.Style;

import org.htmlcleaner.TagNode;

public class HorizontalLineHandler extends com.sysdata.htmlspanner.handlers.attributes.HorizontalLineHandler {

    private final Drawable drawable;
    private static final int[] ATTRS = new int[]{ android.R.attr.listDivider };

    public HorizontalLineHandler(StyledTextHandler handler, Context context) {
        super(handler);

        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        Drawable divider = a.getDrawable(0);
        if (divider == null) {
            Log.w(HorizontalLineHandler.class.getSimpleName(), "@android:attr/listDivider was not set in the theme used for this "
                    + "HorizontalLineHandler. Please set that attribute; otherwise a default Color.GRAY will be used (per default in HorizontalLineSpan).");
            drawable = null;
        } else {
            drawable = divider.mutate();
        }
        a.recycle();
    }

    @Override
    public void handleTagNode(TagNode node, SpannableStringBuilder builder, int start, int end, Style useStyle, SpanStack spanStack) {
        if (useStyle.getBorderColor() == null && drawable != null) {
            end += 2;
            Log.d("HorizontalLineHandler", "Draw hr with listDivider drawable from " + start + " to " + end);
            spanStack.pushSpan(new DrawableHorizontalLineSpan(useStyle, drawable), start, end);
            appendNewLine(builder);
        } else super.handleTagNode(node, builder, start, end, useStyle, spanStack);
    }
}

package godau.fynn.moodledirect.util;

import android.app.Application;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Build;

import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.data.persistence.UserAccount;
import godau.fynn.moodledirect.network.NetworkStateReceiver;

import static android.net.ConnectivityManager.CONNECTIVITY_ACTION;

public class MyApplication extends Application {

    private static MyApplication instance;
    private MoodleDatabase moodle;
    private boolean isDarkMode = false;

    public final NetworkStateReceiver networkStateReceiver = new NetworkStateReceiver();

    public static synchronized MyApplication getInstance() {
        return instance;
    }

    public static MoodleDatabase moodle() {
        if (instance.moodle == null) instance.moodle = MoodleDatabase.get(instance);
        return instance.moodle;
    }

    public static void commitDatabase() {
        moodle().close();
        instance.moodle = null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        PreferenceHelper preferences = new PreferenceHelper(this);
        if (preferences.isLoggedIn()) {
            Constants.API_URL = preferences.getUserAccount().getUrl();
        }

        networkStateReceiver.onUpdateForce(preferences.isForceOfflineModeEnabled());

        this.isDarkMode = preferences.isDarkThemeEnabled();

        ConnectivityManager connectivityManager = ((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager.registerDefaultNetworkCallback(networkStateReceiver);
        } else {
            connectivityManager.registerNetworkCallback(new NetworkRequest.Builder().build(), networkStateReceiver);
        }
        networkStateReceiver.update(connectivityManager);
    }

    public boolean isDarkModeEnabled() {
        return this.isDarkMode;
    }

    public void setDarkModeEnabled(boolean isEnabled) {
        this.isDarkMode = isEnabled;
    }
}
package godau.fynn.moodledirect.network;

import godau.fynn.moodledirect.model.api.CalendarToken;
import godau.fynn.moodledirect.model.api.Zoom;
import godau.fynn.moodledirect.model.api.SiteInformation;
import godau.fynn.moodledirect.model.api.UserToken;
import godau.fynn.moodledirect.model.api.assign.SubmissionStatus;
import godau.fynn.moodledirect.model.api.base.Category;
import godau.fynn.moodledirect.model.api.base.CourseList;
import godau.fynn.moodledirect.model.api.base.ServiceQuery;
import godau.fynn.moodledirect.model.api.base.ServiceResponse;
import godau.fynn.moodledirect.model.api.choice.ChoiceDetailsList;
import godau.fynn.moodledirect.model.api.choice.ChoiceOptionsList;
import godau.fynn.moodledirect.model.api.choice.ChoiceResultList;
import godau.fynn.moodledirect.model.api.enrol.EnrolmentOption;
import godau.fynn.moodledirect.model.api.enrol.GuestEnrolWrapper;
import godau.fynn.moodledirect.model.api.enrol.SelfEnrolmentOption;
import godau.fynn.moodledirect.model.api.tool.AutoLogin;
import godau.fynn.moodledirect.model.api.tool.MobileConfig;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;
import godau.fynn.moodledirect.model.database.CourseSection;
import godau.fynn.moodledirect.model.api.enrol.SelfEnrolResult;
import godau.fynn.moodledirect.model.api.forum.ForumData;

import java.util.List;
import java.util.Map;

/**
 * Created by harsu on 16-12-2016.
 */

public interface MoodleServices {

    /**
     * @param query Must be {@code [{"index":0, "methodname":"tool_mobile_get_public_config","args":{}}]}
     */
    @POST("lib/ajax/service.php")
    Call<List<ServiceResponse.PublicConfig>> getMobileConfig(@Body List<ServiceQuery> query);

    /**
     * User's data like username, first name, last name, full name, userId is obtained.
     * Also contains information about calls supported by the webservice.
     */
    @GET("webservice/rest/server.php?wsfunction=core_webservice_get_site_info&moodlewsrestformat=json")
    Call<SiteInformation> getSiteInformation(@Query("wstoken") String token);

    /**
     * Gathers mobile-specific configuration. Most interestingly, it contains a value for
     * <code>tool_mobile_autologinmintimebetweenreq</code>, i. e. the amount of time that needs
     * to be waited between autologin attempts.
     */
    @POST("webservice/rest/server.php?wsfunction=tool_mobile_get_config&moodlewsrestformat=json")
    Call<MobileConfig> getMobileConfig(@Query("wstoken") String token);

    @POST("login/token.php?service=moodle_mobile_app&moodlewsrestformat=json")
    Call<UserToken> getTokenByLogin(@Query("username") String username,
                                    @Query("password") String password);

    /**
     * @param query Must be {@code [{"index":0, "methodname":"tool_mobile_get_tokens_for_qr_login","args":{"qrloginkey":"…","userid":"…"}}]}
     */
    @POST("lib/ajax/service.php")
    @Headers("User-Agent: MoodleMobile") // Otherwise, apprequired exception is thrown
    Call<List<ServiceResponse.UserToken>> callServiceForTokenByQr(@Body List<ServiceQuery> query);

    @GET("lib/ajax/service.php?info=tool_mobile_get_public_config")
    Call<ResponseBody> getPublicConfig();

    @GET("webservice/rest/server.php?wsfunction=core_calendar_get_calendar_export_token&moodlewsrestformat=json")
    Call<CalendarToken> getCalendarExportToken(@Query("wstoken") String token);

    @GET("webservice/rest/server.php?wsfunction=core_webservice_get_site_info&moodlewsrestformat=json")
    Call<ResponseBody> checkToken(@Query("wstoken") String token);

    @GET("webservice/rest/server.php?wsfunction=core_enrol_get_users_courses&moodlewsrestformat=json")
    Call<List<Course>> getCourses(@Query("wstoken") String token, @Query("userid") int userID);

    @GET("webservice/rest/server.php?wsfunction=core_course_get_courses_by_field&moodlewsrestformat=json&field=id")
    Call<CourseList> getCourseById(@Query("wstoken") String token, @Query("value") int courseId);

    /**
     * @param categoryId Category ID
     * @see <a href="https://github.com/moodle/moodle/blob/e746dc75af1f537e56b366b6368ae3f33482687f/course/externallib.php#L1704-L1728">Moodle source code</a>,
     * <a href="https://gist.github.com/phette23/f0289c3d9883b6e94e4a5308341459e4">Python script</a>
     */
    @GET("webservice/rest/server.php?wsfunction=core_course_get_categories&moodlewsrestformat=json&criteria[0][key]=id&addsubcategories=0")
    Call<ResponseBody> getCourseCategory(@Query("wstoken") String token, @Query("criteria[0][value]") int categoryId);

    /**
     * @param categoryIds Comma-separated list
     * @see <a href="https://github.com/moodle/moodle/blob/e746dc75af1f537e56b366b6368ae3f33482687f/course/externallib.php#L1704-L1728">Moodle source code</a>,
     * <a href="https://gist.github.com/phette23/f0289c3d9883b6e94e4a5308341459e4">Python script</a>
     */
    @GET("webservice/rest/server.php?wsfunction=core_course_get_categories&moodlewsrestformat=json&criteria[0][key]=ids&addsubcategories=0")
    Call<Category[]> getCourseCategories(@Query("wstoken") String token, @Query("criteria[0][value]") String categoryIds);

    @GET("webservice/rest/server.php?wsfunction=core_course_get_contents&moodlewsrestformat=json")
    Call<List<CourseSection>> getCourseContent(@Query("wstoken") String token,
                                               @Query("courseid") int courseID);

    /**
     * Obtains a module from a module's instance ID.
     *
     * @see <a href="https://tracker.moodle.org/browse/MDL-51579">Issue that introduces this call</a>
     * @since Moodle 3.0
     */
    @GET("webservice/rest/server.php?wsfunction=core_course_get_course_module_by_instance&moodlewsrestformat=json")
    Call<Module> getModuleByInstance(
        @Query("wstoken") String token,
        @Query("instance") int instance,
        @Query("module") String moduleResourceIdentifier
    );

    @GET("webservice/rest/server.php?wsfunction=core_course_search_courses&moodlewsrestformat=json&criterianame=search&perpage=250")
    Call<CourseList> searchCourses(@Query("wstoken") String token,
                                     @Query("criteriavalue") String courseName);

    @GET("webservice/rest/server.php?wsfunction=core_enrol_get_course_enrolment_methods&moodlewsrestformat=json")
    Call<List<EnrolmentOption>> getEnrolmentMethods(@Query("wstoken") String token,
                                                    @Query("courseid") int courseId);

    @GET("webservice/rest/server.php?wsfunction=enrol_self_get_instance_info&moodlewsrestformat=json")
    Call<SelfEnrolmentOption> getSelfEnrolmentInstanceInformation(@Query("wstoken") String token,
                                                                  @Query("instanceid") int id);

    @GET("webservice/rest/server.php?wsfunction=enrol_guest_get_instance_info&moodlewsrestformat=json")
    Call<GuestEnrolWrapper> getGuestEnrolmentInstanceInformation(@Query("wstoken") String token,
                                                                 @Query("instanceid") int id);

    @GET("webservice/rest/server.php?wsfunction=enrol_self_enrol_user&moodlewsrestformat=json")
    Call<SelfEnrolResult> selfEnrolUserInCourse(@Query("wstoken") String token,
                                                @Query("courseid") int courseId,
                                                @Query("instanceid") int instanceId,
                                                @Query("password") String password);

    @GET("webservice/rest/server.php?wsfunction=mod_forum_get_forum_discussions&moodlewsrestformat=json")
    Call<ForumData> getForumDiscussions(@Query("wstoken") String token,
                                        @Query("forumid") int forumInstance);

    @POST("webservice/rest/server.php?moodlewsrestformat=json&wsfunction=mod_forum_get_discussion_posts&moodlewssettingfilter=true&moodlewssettingfileurl=true")
    Call<ForumData> getForumDiscussion(@Query("wstoken") String token,
                                          @Query("discussionid") int id);

    @POST("webservice/rest/server.php?moodlewsrestformat=json&wsfunction=mod_choice_get_choice_options&moodlewssettingfilter=true&moodlewssettingfileurl=true")
    Call<ChoiceOptionsList> getChoiceOptions(@Query("wstoken") String token,
                                             @Query("choiceid") int choiceId);

    @POST("webservice/rest/server.php?moodlewsrestformat=json&wsfunction=mod_choice_submit_choice_response&moodlewssettingfilter=true&moodlewssettingfileurl=true")
    Call<ResponseBody> submitChoiceResponse(@Query("wstoken") String token,
                                            @Query("choiceid") int choiceId,
                                            @QueryMap() Map<String, Integer> responses);

    @POST("webservice/rest/server.php?moodlewsrestformat=json&wsfunction=mod_choice_get_choices_by_courses&moodlewssettingfileurl=true&moodlewssettingfilter=true")
    Call<ChoiceDetailsList> getChoiceDetails(@Query("wstoken") String token,
                                             @Query("courseids[0]") int courseId);

    @POST("webservice/rest/server.php?moodlewsrestformat=json&wsfunction=mod_choice_delete_choice_responses")
    Call<ResponseBody> undoChoiceResponse(@Query("wstoken") String token,
                                          @Query("choiceid") int choiceid);

    @POST("webservice/rest/server.php?moodlewsrestformat=json&wsfunction=mod_choice_get_choice_results")
    Call<ChoiceResultList> getChoiceResults(@Query("wstoken") String token,
                                            @Query("choiceid") int choiceid);

    @POST("webservice/rest/server.php?moodlewsrestformat=json&wsfunction=mod_assign_get_submission_status")
    Call<SubmissionStatus> getSubmissionStatus(@Query("wstoken") String token,
                                               @Query("assignid") int assignment);

    @GET("webservice/rest/server.php?moodlewsrestformat=json&wsfunction=mod_zoom_grade_item_update")
    Call<Zoom> getZoom(@Query("zoomid") int id, @Query("wstoken") String token);

    @POST("webservice/rest/server.php?moodlewsrestformat=json&wsfunction=tool_mobile_get_autologin_key")
    @FormUrlEncoded
    @Headers("User-Agent: MoodleMobile") // Otherwise, apprequired exception is thrown
    Call<AutoLogin> getAutoLoginKey(@Query("wstoken") String token,
                                    @Field("privatetoken") String privateToken);

    @GET
    Call<ResponseBody> getFile(@Url String url, @Query("token") String token);

}

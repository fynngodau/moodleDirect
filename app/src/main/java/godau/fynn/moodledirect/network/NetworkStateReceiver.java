package godau.fynn.moodledirect.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.strictmode.NonSdkApiUsedViolation;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.LinkedList;

public class NetworkStateReceiver extends ConnectivityManager.NetworkCallback {

    private static boolean noInternet = false;

    /**
     * True if the preference for offline mode has been enabled.
     */
    private static boolean forceOffline;

    private final LinkedList<OfflineStatusChangeListener> listeners = new LinkedList<>();

    @Override
    public void onLost(@NonNull Network network) {
        noInternet = true;
        broadcast();
        Log.d(NetworkStateReceiver.class.getSimpleName(), "Network was lost");
    }

    @Override
    public void onCapabilitiesChanged(@NonNull Network network, @NonNull NetworkCapabilities networkCapabilities) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            noInternet = !networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED);
        } else {
            noInternet = !networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET);
        }
        Log.d(NetworkStateReceiver.class.getSimpleName(), "Network capabilities have changed. noInternet = " + noInternet + " (SDK version " + Build.VERSION.SDK_INT + ")");

        broadcast();
    }

    @Override
    public void onUnavailable() {
        super.onUnavailable();
        Log.d(NetworkStateReceiver.class.getSimpleName(), "Network has become unavailable");
    }

    public void onUpdateForce(boolean force) {
        forceOffline = force;
        broadcast();
    }

    public void update(ConnectivityManager connectivityManager) {
        final NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        noInternet = info == null || !info.isConnected();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            noInternet = noInternet ||
                    !connectivityManager
                            .getNetworkCapabilities(connectivityManager.getActiveNetwork())
                            .hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED);
        }

        Log.d(NetworkStateReceiver.class.getSimpleName(), "Determined that network is unavailable");
        broadcast();
    }

    private void broadcast() {
        for (OfflineStatusChangeListener listener : listeners) {
            if (listener instanceof Context) {
                new Handler(Looper.getMainLooper()).post(() -> listener.onNewOfflineStatus(noInternet | forceOffline));
            } else {
                listener.onNewOfflineStatus(noInternet | forceOffline);
            }
        }
    }

    /**
     * Informs the listener about future change events, and immediately dispatches the current
     * status.
     */
    public void addListener(OfflineStatusChangeListener listener) {
        listeners.add(listener);

        listener.onNewOfflineStatus(noInternet | forceOffline);
    }

    public void removeListener(OfflineStatusChangeListener listener) {
        listeners.remove(listener);
    }

    /**
     * @return True if the app should work offline (the app is "effectively" offline).
     */
    public static boolean getOfflineStatus() {
        return noInternet | forceOffline;
    }

    public interface OfflineStatusChangeListener {
        void onNewOfflineStatus(boolean offline);
    }
}

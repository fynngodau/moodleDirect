package godau.fynn.moodledirect.module.link;

import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.module.choice.ChoiceFragmentFactory;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;

public class ChoiceLink extends ModuleLink {
    @Override
    protected void onOpen(Module module, Course course, FragmentActivity context, View drawContext) {
        Fragment choiceFragment = ChoiceFragmentFactory.makeChoiceFragment(module.getInstance(), module.courseId);
        showFragment(context, choiceFragment, "Choice");
    }

    @Override
    protected String[] requiresCalls() {
        // Available from Moodle 3.0
        return new String[]{
                "mod_choice_get_choice_options",
                "mod_choice_submit_choice_response",
                "mod_choice_get_choices_by_courses",
                "mod_choice_delete_choice_responses"
        };
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_poll;
    }

    @Override
    public int getName() {
        return R.string.choice;
    }

    @Override
    public int getRequirementText() {
        return R.string.supported_modules_requirement_moodle_30;
    }
}

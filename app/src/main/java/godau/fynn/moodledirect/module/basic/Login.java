package godau.fynn.moodledirect.module.basic;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.core.util.Pair;

import com.google.gson.stream.MalformedJsonException;

import godau.fynn.moodledirect.model.api.MoodleException;
import godau.fynn.moodledirect.model.api.SiteInformation;
import godau.fynn.moodledirect.model.api.UserToken;
import godau.fynn.moodledirect.model.api.base.PublicConfig;
import godau.fynn.moodledirect.model.api.base.ServiceQuery;
import godau.fynn.moodledirect.model.api.base.ServiceResponse;
import godau.fynn.moodledirect.network.APIClient;
import godau.fynn.moodledirect.network.exception.NotOkayException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Login is not a ModuleD because a database connection is not yet established during
 * login process.
 */
public class Login {

    private final Context context;

    public Login(Context context) {
        this.context = context;
    }

    public PublicConfig getPublicConfig(String url) throws IOException {

        // Attempt to treat provided URL as root URL
        try {
            return attemptGetPublicConfig(url);
        } catch (NotOkayException e) {
            // NOOP
        }

        // If this fails, attempt to follow redirects using "native" OKHttps
        Response response = new OkHttpClient().newCall(new Request.Builder()
                .head()
                .url(url)
                .build()
        ).execute();
        url = response.request().url().toString();
        Log.d(Login.class.getSimpleName(), "HEAD query delivered URL " + url);

        // Then attempt to query server config again, from true root URL this time
        return attemptGetPublicConfig(url);
    }

    private PublicConfig attemptGetPublicConfig(String url) throws NotOkayException, IOException {
        if (!(url.endsWith("/") || url.contains("/?")) ) {
            if (url.contains("?")) url = url.replace("?", "/?");
            else url = url + "/";
        }
        try {
            List<ServiceResponse.PublicConfig> responses = APIClient.makeServices(url)
                    .getMobileConfig(Collections.singletonList(
                            new ServiceQuery(0, "tool_mobile_get_public_config", new ServiceQuery.Args()))
                    )
                    .execute().body();


            if (responses.size() < 1) throw new NotOkayException();
            if (responses.get(0) == null || responses.get(0).data == null || responses.get(0).data.getHttpswwwroot() == null)
                throw new NotOkayException();
            if (responses.get(0).data.isMobileAppServiceEnabled == 0)
                throw new MoodleException("enablewsdescription", null);

            return responses.get(0).data;
        } catch (MalformedJsonException e) {
            throw new NotOkayException();
        }
    }

    /**
     *
     * @param config   Moodle server configuration
     * @param username Basic auth username
     * @param password Basic auth password
     * @return An authenticated token if successful.
     * @throws MoodleException If authentication is unsuccessful.
     * @throws NotOkayException If authentication is unsuccessful in an unexpected way.
     * @throws IOException If a network error occurs.
     */
    public UserToken basicAuth(PublicConfig config, String username, String password)
            throws MoodleException, NotOkayException, IOException {
        UserToken token = APIClient.makeServices(config.getHttpswwwroot())
                .getTokenByLogin(username, password)
                .execute().body();

        if (token.token == null || token.token.length() == 0) throw new NotOkayException();

        return token;
    }

    public Pair<PublicConfig, UserToken> qrCodeAuth(Uri uri) throws IOException {

        PublicConfig config = getPublicConfig(uri.toString().replaceAll("moodlemobile://", ""));

        // Build query
        ServiceQuery query = new ServiceQuery(0, "tool_mobile_get_tokens_for_qr_login", new ServiceQuery.Args(
                uri.getQueryParameter("qrlogin"), uri.getQueryParameter("userid")
        ));

        UserToken token = APIClient.makeServices(config.getHttpswwwroot())
                .callServiceForTokenByQr(Collections.singletonList(query))
                .execute().body().get(0).data;

        return new Pair<>(config, token);
    }

    public SiteInformation getUserDetail(PublicConfig config, UserToken token) throws IOException {
        return APIClient.makeServices(config.getHttpswwwroot())
                .getSiteInformation(token.token)
                .execute().body();
    }
}

package godau.fynn.moodledirect.module.basic;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import godau.fynn.moodledirect.OfflineException;
import godau.fynn.moodledirect.model.ResourceType;
import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.model.database.CourseSection;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.module.ModuleD;
import godau.fynn.moodledirect.module.files.FileSupport;

public abstract class CourseContent extends ModuleD {

    public abstract List<Module> getCourseModules(int courseId) throws IOException;

    public abstract Module getModule(int moduleId, ResourceType resourceType) throws IOException;

    @Dao
    public static abstract class Offline extends CourseContent {

        @Query("SELECT * FROM module WHERE courseId = :courseId ORDER BY orderNumber ASC")
        protected abstract List<Module> getModules(int courseId);

        @Query("SELECT * FROM file WHERE reference = :moduleId ORDER BY orderNumber ASC")
        protected abstract List<File> getFiles(int moduleId);

        @Query("SELECT * FROM module WHERE instance = :instance AND moduleType = :resourceType")
        protected abstract Module getModuleByInstance(int instance, String resourceType);

        @Transaction
        @Override
        public List<Module> getCourseModules(int courseId) {

            List<Module> modules = getModules(courseId);

            for (int i = 0; i < modules.size(); i++) {
                Module module = modules.get(i);
                if (module.moduleType.equals("core_course_section")) {
                    modules.set(i, new CourseSection(module));
                } else {
                    module.contents = getFiles(module.id);
                }
            }

            database.getFile().wrap().addDownloadStatus(modules);

            if (modules.isEmpty()) throw new OfflineException();

            return modules;
        }

        @Transaction
        @Override
        public Module getModule(int instance, ResourceType resourceType) throws IOException {
            Module module = getModuleByInstance(instance, resourceType.id);

            if (module == null) return null;

            if (module.moduleType.equals("core_course_section")) {
                return new CourseSection(module);
            } else {
                module.contents = getFiles(module.id);

                database.getFile().wrap().addDownloadStatus(Collections.singletonList(module));

                return module;
            }
        }
    }

    @Dao
    public static abstract class Online extends CourseContent {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        protected abstract void insert(List<Module> modules, List<File> files);

        @Query("DELETE FROM file WHERE reference IN (SELECT id FROM module WHERE courseId = :courseId)")
        protected abstract void deleteFilesForCourse(int courseId);

        @Query("DELETE FROM module WHERE courseId = :courseId")
        protected abstract void deleteModulesForCourse(int courseId);

        @Override
        public Module getModule(int instance, ResourceType resourceType) throws IOException {

            throw new UnsupportedOperationException(
                    "This operation is not available in online mode due to lacking API calls."
            );
            /*
            Module module = moodleServices.getModuleByInstance(userAccount.getToken(), instance, resourceType.id)
                    .execute().body();

            // Assemble files
            List<File> fileList = new ArrayList<>();
            if (module.contents != null) {
                int orderNumber = 1;
                for (File file : module.contents) {
                    file.reference = module.id;
                    file.orderNumber = orderNumber++;
                }
                insert(Collections.emptyList(), fileList);
            }

            // Lacking context, and therefore and appropriate orderNumber, we cannot store the module to the database.

            database.getFile().wrap().addDownloadStatus(Collections.singletonList(module));

            return module;*/
        }

        @Transaction
        protected void setCourseData(List<Module> modules, List<File> files, int courseId) {
            deleteFilesForCourse(courseId);
            deleteModulesForCourse(courseId);
            insert(modules, files);
        }

        @Override
        public List<Module> getCourseModules(int courseId) throws IOException {

            List<CourseSection> courseSections = moodleServices.getCourseContent(userAccount.getToken(), courseId)
                    .execute().body();

            // Filter empty sections
            for (int i = courseSections.size() - 1; i >= 0; i--) {
                CourseSection section = courseSections.get(i);
                if ((section.getModules() == null || section.getModules().isEmpty())
                        && (section.getSummary() == null || section.getSummary().isEmpty())
                        && section.available // announce future sections
                ) {
                    courseSections.remove(section);
                }
            }

            // Flatten CourseSections
            List<Module> moduleList = new ArrayList<>();

            for (CourseSection section : courseSections) {
                section.moduleType = "core_course_section";
                moduleList.add(section);
                moduleList.addAll(section.modules);
            }

            // Assemble files
            List<File> fileList = FileSupport.buildFileList(new ArrayList<>(moduleList));

            // Count through modules
            int orderNumber = 1;
            for (Module module : moduleList) {
                module.orderNumber = orderNumber++;
                module.courseId = courseId;
            }

            setCourseData(moduleList, fileList, courseId);

            database.getFile().wrap().addDownloadStatus(moduleList);

            return moduleList;
        }
    }

}

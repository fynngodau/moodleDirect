package godau.fynn.moodledirect.module;

import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.room.*;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.database.file.DownloadedFile;
import godau.fynn.moodledirect.network.Validator;
import godau.fynn.moodledirect.util.FileManagerWrapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSink;
import okio.Okio;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Dao
public abstract class FileManager extends ModuleD {

    protected FileManager() {
    }

    @Query("SELECT * FROM downloadedFile WHERE url = :fileUrl")
    protected abstract DownloadedFile getLocation(String fileUrl);


    @Query("SELECT * FROM downloadedFile WHERE url IN (:fileUrls)")
    protected abstract List<DownloadedFile> findFiles(List<String> fileUrls);

    @Query("SELECT * FROM downloadedFile")
    public abstract List<DownloadedFile> getAllFiles();

    @Query("DELETE FROM downloadedFile")
    public abstract Void clear();


    public Uri getLocation(godau.fynn.moodledirect.model.api.file.File file) {
        DownloadedFile downloadedFile = this.getLocation(file.url);

        if (downloadedFile != null && exists(downloadedFile)) {
            return downloadedFile.location;
        } else {
            // Display failed state
            file.downloadStatus = DownloadStatus.FAILED;

            return null;
        }
    }

    @Delete
    protected abstract int remove(DownloadedFile file);

    /**
     * Tests which of the provided files have already been downloaded and marks
     * each provided File accordingly. Does not know whether files are currently downloading.
     *
     * <p>Warning: do not pass too many files, else we will receive an SQLiteException.
     */
    @Transaction
    public void addDownloadStatus(List<godau.fynn.moodledirect.model.api.file.File> files) {

        // Find DownloadedFile objects for matching fileUrls
        List<DownloadedFile> downloadedFilesSubset = findFiles(
                files.stream().map(file -> file.url).collect(Collectors.toList())
        );

        // Verify that files are actually present
        for (int i = downloadedFilesSubset.size() - 1; i >= 0; i--) {
            DownloadedFile downloadedFile = downloadedFilesSubset.get(i);
            if (!exists(downloadedFile)) {
                downloadedFilesSubset.remove(downloadedFile);
            }
        }

        // Find updated files
        iterateOverProvidedFiles:
        for (godau.fynn.moodledirect.model.api.file.File file : files) {
            for (DownloadedFile downloadedFile : downloadedFilesSubset) {
                if (file.url.equals(downloadedFile.url)) {

                    // Detect updates
                    if (downloadedFile.timeModified < file.timeModified) {
                        file.downloadStatus = DownloadStatus.UPDATE_AVAILABLE;
                    } else {
                        // No update available
                        file.downloadStatus = DownloadStatus.DOWNLOADED;
                    }
                    continue iterateOverProvidedFiles;
                }
            }

            // Searched through all DownloadedFiles, yet no match was found
            file.downloadStatus = DownloadStatus.NOT_DOWNLOADED;
        }
    }

    /**
     * Tests if file exists on file system
     */
    private boolean exists(@NonNull DownloadedFile downloadedFile) {
        DocumentFile file = DocumentFile.fromSingleUri(context, downloadedFile.location);
        return file != null && file.exists();
    }

    public Uri getDownloadUri() {
        String path = preferences.getDownloadPath();
        if (path == null) return null;
        else return Uri.parse(path);
    }

    public FileManagerWrapper wrap() {
        return new FileManagerWrapper(this, context);
    }

    @NonNull
    public static String getExtension(String filename) {
        return filename.substring(filename.lastIndexOf('.') + 1);
    }

    public static int getIconFromFileName(String filename) {
        switch (FileManager.getExtension(filename)) {
            case "pdf":
                return (R.drawable.file_pdf);

            case "xls":
            case "xlsx":
                return (R.drawable.file_excel);

            case "doc":
            case "docx":
                return (R.drawable.file_word);

            case "ppt":
            case "pptx":
                return (R.drawable.file_powerpoint);

            default:
                return Resources.ID_NULL;
        }
    }

    public Uri download(godau.fynn.moodledirect.model.api.file.File file, String folderName) throws IOException {
        // Search for file in database
        Uri location = getLocation(file);
        if (location == null)
            throw new FileNotFoundException("File not available on file system – offline mode enabled");
        else return location;
    }

    public Uri download(godau.fynn.moodledirect.model.api.file.File file, String courseFolderName, String moduleFolderName) throws IOException {
        // We are in offline mode, we can only consult the database where the file is (independently of folder names).
        return download(file, courseFolderName);
    }

    @Dao
    public static abstract class Online extends FileManager {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        public abstract void insert(DownloadedFile file);

        @Override
        public Uri download(godau.fynn.moodledirect.model.api.file.File file, String folderName, String moduleFolderName) throws IOException {
            String url;
            if (userAccount.getUrl().endsWith("webservice/") || userAccount.getUrl().contains("webservice/pluginfile.php")) {
                // Fallback for "confusing" Moodle instances – does not work for forum attachments
                url = file.url + "?&token=" + userAccount.getToken();
            } else {
                // Insert file download token into URL
                url = file.url
                        .replace("/webservice/pluginfile.php/", "/pluginfile.php/")
                        .replace("/pluginfile.php/",
                                "/tokenpluginfile.php/" +
                                        userAccount.getFilePrivateAccessKey() + "/"
                        );
            }

            DocumentFile rootDirectory = DocumentFile.fromTreeUri(context, getDownloadUri());

            DocumentFile directory = changeToDirectory(rootDirectory, folderName);


            // FOLDER module folder
            if (moduleFolderName != null) {
                directory = changeToDirectory(directory, moduleFolderName);
            }

            // Create file
            DocumentFile documentFile = directory.createFile(
                    file.mimetype,
                    // Remove invalid characters (or characters that may be invalid on some systems)
                    file.filename.replaceAll("[|\\\\?*<\":>+\\[\\]/']", "")
            );

            if (documentFile == null) {
                throw new FileCreationException();
            }

            // Prepare writing to file
            BufferedSink sink = Okio.buffer(
                    Okio.sink(
                    context.getContentResolver().openOutputStream(documentFile.getUri())
                    )
            );

            // Download file
            Response response = new OkHttpClient().newCall(
                    new Request.Builder()
                            .url(url)
                            .get()
                            .build()
            ).execute();

            // Test for errors (including whether body is null)
            Validator.validate(response);

            // Store file
            sink.writeAll(response.body().source());
            sink.close();
            response.close();

            // Store to database that file was downloaded
            insert(new DownloadedFile(file, documentFile.getUri()));

            return documentFile.getUri();
        }

        /**
         * @return The requested directory
         * @throws IllegalStateException if the directory does not exist and could not be created
         */
        private DocumentFile changeToDirectory(DocumentFile documentFile, String name) throws IllegalStateException {

            // Remove invalid characters (or characters that may be invalid on some systems)
            name = name.trim().replaceAll("[|\\\\?*<\":>+\\[\\]/']", "");

            // Find directory
            DocumentFile directory = null;
            for (DocumentFile childFile : documentFile.listFiles()) {
                if (name.equals(childFile.getName())) {
                    directory = childFile;
                }
            }

            // File with directory name
            if (directory != null && !directory.isDirectory()) {
                Log.d(FileManager.class.getSimpleName(),
                        name + " was a file, not a directory. Renamed to "
                                + name + ".file (success = " +
                                directory.renameTo(name + ".file")
                                + ")"
                );
                directory = null;
            }

            if (directory == null) {
                // Create directory
                Log.d(FileManager.class.getSimpleName(),
                        "Directory " + name + " not found so it was created. (failure = "
                                + ((directory = documentFile.createDirectory(name)) == null)
                                + ")"
                );
            }

            if (directory == null) throw new DirectoryCreationException();

            return directory;

        }

        @Override
        public Uri download(godau.fynn.moodledirect.model.api.file.File file, String courseFolderName) throws IOException {
            return super.download(file, courseFolderName, null);
        }
    }

    public enum DownloadStatus {
        NOT_DOWNLOADED, DOWNLOADING, DOWNLOADED, UPDATE_AVAILABLE, FAILED
    }

    public static class DirectoryCreationException extends IllegalStateException {}

    public static class FileCreationException extends IllegalStateException {}
}

package godau.fynn.moodledirect.module.forum;

import android.text.Html;
import androidx.room.*;
import godau.fynn.moodledirect.OfflineException;
import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.model.api.forum.Discussion;
import godau.fynn.moodledirect.module.ModuleD;
import godau.fynn.moodledirect.module.files.FileSupport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class Forum extends ModuleD {

    public abstract List<Discussion> getDiscussions(int forumInstance) throws IOException;

    public abstract List<Discussion> getPostsByDiscussion(int discussion) throws IOException;

    @Dao
    public static abstract class Online extends Forum {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        protected abstract void insertDiscussions(List<Discussion> discussions);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        protected abstract void insertFiles(List<File> files);

        /**
         * Return only such posts that have the given discussion ID and a non-zero
         * forum id. This represents an object whose data must be merged in the
         * course of {@link #getPostsByDiscussion(int)}.
         */
        @Query("SELECT id, forumId, lastPostUserId, replyCount, pinned FROM discussion " +
                "WHERE forumId > 0 AND discussionId = :discussionId")
        protected abstract List<DiscussionMergeHelper> getPostsByDiscussionWithForumInstance(int discussionId);

        @Override
        public List<Discussion> getDiscussions(int forumInstance) throws IOException {
            //noinspection ConstantConditions
            List<Discussion> discussions = moodleServices
                    .getForumDiscussions(userAccount.getToken(), forumInstance)
                    .execute().body().discussions;

            for (Discussion discussion : discussions) {
                // Attach forum id
                discussion.forumInstance = forumInstance;

                // Resolve characters like &amp;
                discussion.subject = Html.fromHtml(discussion.subject).toString();

                // Normalize attachment links
                if (discussion.attachments != null) for (File file : discussion.attachments) {
                    file.url = file.url.replace("webservice/pluginfile.php", "pluginfile.php");
                }
            }

            // Ensure sorting by creation date descending
            Collections.sort(discussions, ((o1, o2) -> o2.created - o1.created));

            insertDiscussions(discussions);
            insertFiles(FileSupport.buildFileList(new ArrayList<>(discussions)));
            database.getFile().wrap().addDownloadStatus(discussions);

            return discussions;
        }

        @Override
        public List<Discussion> getPostsByDiscussion(int discussionId) throws IOException {
            //noinspection ConstantConditions
            List<Discussion> discussions = moodleServices
                    .getForumDiscussion(userAccount.getToken(), discussionId)
                    .execute().body().discussions;

            // Do not assign forum ID such that not all entries will be found as root posts

            for (Discussion discussion : discussions) {
                // Resolve characters like &amp; and data from author object
                discussion.subject = Html.fromHtml(discussion.subject).toString();
                discussion.userId = discussion.author.id;
                discussion.userName = discussion.author.fullname;
                discussion.userAvatar = discussion.author.urls.profileimage;

                // Normalize attachment links
                if (discussion.attachments != null) for (File file : discussion.attachments) {
                    file.url = file.url.replace("?forcedownload=1", "");
                }
            }


            /* Merge query results (subjects) with existing database items that also represent a discussion
             * thread and therefore have additional data.
             */
            for (DiscussionMergeHelper item : getPostsByDiscussionWithForumInstance(discussionId)) {
                for (Discussion subject : discussions) {
                    if (item.id == subject.id) {
                        // Copy from item to subject
                        subject.forumInstance = item.forumInstance;
                        subject.lastPostUserId = item.lastPostUserId;
                        subject.replyCount = item.replyCount;
                        subject.pinned = item.pinned;
                    }
                }
            }

            /* Sort by (1) hierarchy then (2) creation time ascending (new posts at the bottom)
             * Like when implementing radix sort we will perform two distinct, stable sort operations,
             * and we need to start with the "least significant bit"; here that is condition (2)
             * as it has lower priority.
             */
            Collections.sort(discussions, Comparator.comparingInt(o -> o.created));
            discussions = Forum.hierarchySort(discussions);

            insertDiscussions(discussions);
            insertFiles(FileSupport.buildFileList(new ArrayList<>(discussions)));
            database.getFile().wrap().addDownloadStatus(discussions);
            return discussions;
        }
    }

    @Dao
    public static abstract class Offline extends Forum {

        @Query("SELECT * FROM discussion WHERE forumId = :forumInstance ORDER BY created DESC")
        protected abstract List<Discussion> getDiscussionsInternal(int forumInstance);

        @Query("SELECT * FROM discussion WHERE discussionId = :discussion ORDER BY created ASC")
        protected abstract List<Discussion> getPostsByDiscussionInternal(int discussion);

        @Query("SELECT * FROM file WHERE reference = ('discussion' || :discussionId) ORDER BY orderNumber ASC")
        protected abstract List<File> getFilesForDiscussion(int discussionId);

        @Transaction
        public List<Discussion> getDiscussions(int forumInstance) {
            List<Discussion> discussions = getDiscussionsInternal(forumInstance);
            if (discussions.isEmpty()) throw new OfflineException();
            for (Discussion discussion : discussions) {
                discussion.attachments = getFilesForDiscussion(discussion.discussionId);
            }
            database.getFile().wrap().addDownloadStatus(discussions);

            return discussions;
        }

        @Transaction
        public List<Discussion> getPostsByDiscussion(int discussionId) {
            List<Discussion> discussions = getPostsByDiscussionInternal(discussionId);
            if (discussions.isEmpty()) throw new OfflineException();
            for (Discussion discussion : discussions) {
                discussion.attachments = getFilesForDiscussion(discussion.discussionId);
            }
            database.getFile().wrap().addDownloadStatus(discussions);

            // SQL call returns data sorted by creation time
            return Forum.hierarchySort(discussions);
        }

    }

    /**
     * <b>Caution</b>: this algorithm works in O(n³) with n as the amount of items in the list.
     * It is not possible to use <code>Collections.sort</code>, because the "is parent of"
     * relation does not provide a total ordering.
     *
     * @param discussions List of discussions <b>sorted by creation time ascending</b>
     */
    private static List<Discussion> hierarchySort(List<Discussion> discussions) {
        if (discussions.size() == 0) return discussions;

        // We assume the first object to have no parent object.
        assert discussions.get(0).parent == 0;

        // In this new list we will order our discussion objects.
        List<Discussion> orderedDiscussions = new ArrayList<>();

        Discussion justInserted = new Discussion(); // Dummy discussion with ID 0

        do {
            // Find new object to insert
            justInserted = findDiscussionWithParent(discussions, orderedDiscussions);
            orderedDiscussions.add(justInserted);
            discussions.remove(justInserted);

        } while (discussions.size() > 0);

        return orderedDiscussions;
    }

    /**
     * @return The first discussion in the provided list with the next appropriate parent ID
     * for a threaded view if it exists (it should), the first item otherwise.
     * Enriches the provided item with an appropriate depth value.
     */
    private static Discussion findDiscussionWithParent(List<Discussion> discussions, List<Discussion> hierarchicalList) {
        for (int i = hierarchicalList.size() - 1; i >= 0; i--) {
            Discussion parent = hierarchicalList.get(i);

            for (Discussion discussion : discussions)
                if (discussion.parent == parent.id) {
                    discussion.depth = parent.depth + 1;
                    return discussion;
                }
        }


        return discussions.get(0);
    }
}

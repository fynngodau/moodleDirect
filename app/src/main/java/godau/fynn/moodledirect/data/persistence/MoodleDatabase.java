package godau.fynn.moodledirect.data.persistence;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.AutoMigration;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;
import godau.fynn.moodledirect.OfflineException;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.api.choice.ChoiceDetails;
import godau.fynn.moodledirect.model.api.choice.ChoiceOption;
import godau.fynn.moodledirect.model.api.choice.ChoiceResult;
import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.model.database.file.DownloadedFile;
import godau.fynn.moodledirect.model.api.forum.Discussion;
import godau.fynn.moodledirect.module.*;
import godau.fynn.moodledirect.module.basic.*;
import godau.fynn.moodledirect.module.forum.Forum;
import godau.fynn.moodledirect.network.NetworkStateReceiver;

@Database(
        entities = {
                Course.class,
                Module.class,
                File.class,
                DownloadedFile.class,
                ChoiceDetails.class,
                ChoiceOption.class,
                ChoiceResult.class,
                Discussion.class,
        },
        version = 20,
        autoMigrations = {
                @AutoMigration(from = 17, to = 18)
        },
        exportSchema = true
)
public abstract class MoodleDatabase extends RoomDatabase {

    private Offline offlineDispatch;
    private Online onlineDispatch;

    private Context context;

    public static MoodleDatabase get(Context context) {
        MoodleDatabase database = Room.databaseBuilder(context, MoodleDatabase.class,
                        new PreferenceHelper(context).getUserAccount().getDatabaseName()
                ).addMigrations(
                        MIGRATION_DELETE_INCORRECT_FORUM_FILES,
                        MIGRATION_ADD_COURSE_DATE_FIELDS
                )
                .build();
        database.context = context;

        Log.d(MoodleDatabase.class.getSimpleName(), "Opened new database");
        return database;
    }

    /**
     * Get dispatch according to current effective network state
     */
    public Dispatch getDispatch() {
        if (NetworkStateReceiver.getOfflineStatus()) {
            // Offline dispatch needed
            return forceOffline();
        } else {
            // Online dispatch needed
            if (onlineDispatch == null) return onlineDispatch = new Online();
            else return onlineDispatch;
        }
    }

    /**
     * Get offline mode dispatch
     */
    public Dispatch forceOffline() {
        if (offlineDispatch == null) return offlineDispatch = new Offline();
        else return offlineDispatch;
    }

    protected abstract Core.Online getCoreOnline();
    protected abstract Core.Offline getCoreOffline();
    protected abstract CourseContent.Online getCourseContentOnline();
    protected abstract CourseContent.Offline getCourseContentOffline();
    protected abstract CommonAsset getCommonAssetOffline();
    protected abstract CommonAsset.Online getCommonAssetOnline();
    protected abstract FileManager getFileOffline();
    protected abstract FileManager.Online getFileOnline();
    protected abstract Zoom getZoomOffline();
    protected abstract Zoom.Online getZoomOnline();
    protected abstract Page getPageOffline();
    protected abstract Page.Online getPageOnline();
    protected abstract Choice.Online getChoiceOnline();
    protected abstract Choice.Offline getChoiceOffline();
    protected abstract Forum.Online getForumOnline();
    protected abstract Forum.Offline getForumOffline();
    protected abstract Assignment.Online getAssignmentOnline();
    protected abstract Assignment.Offline getAssignmentOffline();

    public abstract DatabaseCleaner getCleaner();

    public abstract static class Dispatch {
        public abstract Core getCore();
        public abstract CourseContent getCourseContent();
        public abstract Tool getTool();
        public abstract Enrolment getEnrolment();
        public abstract Calendar getCalendar();
        public abstract CommonAsset getCommonAsset();
        public abstract FileManager getFile();
        public abstract Zoom getZoom();
        public abstract Page getPage();
        public abstract Choice getChoice();
        public abstract Forum getForum();
        public abstract Assignment getAssignment();
    }

    public class Online extends Dispatch {
        @Override
        public Core.Online getCore() {
            return (Core.Online) getCoreOnline().inject(context, this);
        }

        @Override
        public CourseContent.Online getCourseContent() {
            return (CourseContent.Online) getCourseContentOnline().inject(context, this);
        }

        @Override
        public Tool getTool() {
            Tool tool = new Tool();
            tool.inject(context, this);
            return tool;
        }

        public Enrolment getEnrolment() {
            Enrolment enrolment = new Enrolment();
            enrolment.inject(context, this);
            return enrolment;
        }

        @Override
        public Calendar getCalendar() {
            Calendar calendar = new Calendar();
            calendar.inject(context, this);
            return calendar;
        }

        @Override
        public CommonAsset getCommonAsset() {
            return (CommonAsset.Online) getCommonAssetOnline().inject(context, this);
        }

        @Override
        public FileManager getFile() {
            return (FileManager) getFileOnline().inject(context, this);
        }

        @Override
        public Zoom getZoom() {
            return (Zoom.Online) getZoomOnline().inject(context, this);
        }

        @Override
        public Page getPage() {
            return (Page) getPageOnline().inject(context, this);
        }

        @Override
        public Choice getChoice() {
            return (Choice) getChoiceOnline().inject(context, this);
        }

        @Override
        public Forum getForum() {
            return (Forum) getForumOnline().inject(context, this);
        }

        @Override
        public Assignment getAssignment() {
            return (Assignment) getAssignmentOnline().inject(context, this);
        }
    }

    public class Offline extends Dispatch {
        @Override
        public Core.Offline getCore() {
            return (Core.Offline) getCoreOffline().inject(context, this);
        }

        @Override
        public CourseContent.Offline getCourseContent() {
            return (CourseContent.Offline) getCourseContentOffline().inject(context, this);
        }

        @Override
        public Tool getTool() {
            throw new OfflineException();
        }

        @Override
        public Enrolment getEnrolment() {
            throw new OfflineException(R.string.exception_offline_enrol);
        }

        @Override
        public Calendar getCalendar() {
            throw new OfflineException(R.string.exception_offline_calendar);
        }

        @Override
        public CommonAsset getCommonAsset() {
            return (CommonAsset) getCommonAssetOffline().inject(context, this);
        }

        @Override
        public FileManager getFile() {
            return (FileManager) getFileOffline().inject(context, this);
        }

        @Override
        public Zoom getZoom() {
            return (Zoom) getZoomOffline().inject(context, this);
        }

        @Override
        public Page getPage() {
            return (Page) getPageOffline().inject(context, this);
        }

        @Override
        public Choice getChoice() {
            return (Choice) getChoiceOffline().inject(context, this);
        }

        @Override
        public Forum getForum() {
            return (Forum) getForumOffline().inject(context, this);
        }

        @Override
        public Assignment getAssignment() {
            return (Assignment) getAssignmentOffline().inject(context, this);
        }
    }

    private static final Migration MIGRATION_DELETE_INCORRECT_FORUM_FILES = new Migration(18, 19) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            Log.d(MoodleDatabase.class.getSimpleName(), "18→19: deleting incorrect forum attachments");
            database.delete("file", "reference LIKE 'discussion%'", null);
        }
    };

    private static final Migration MIGRATION_ADD_COURSE_DATE_FIELDS  = new Migration(19, 20) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            Log.d(MoodleDatabase.class.getSimpleName(), "19→20: adding course date fields");
            database.execSQL("ALTER TABLE course ADD COLUMN `timeStart` INTEGER NOT NULL DEFAULT 0");
            database.execSQL("ALTER TABLE course ADD COLUMN `timeEnd` INTEGER NOT NULL DEFAULT 0");

        }
    };

}

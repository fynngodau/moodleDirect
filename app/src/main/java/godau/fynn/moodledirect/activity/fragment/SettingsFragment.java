package godau.fynn.moodledirect.activity.fragment;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.documentfile.provider.DocumentFile;
import androidx.preference.CheckBoxPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.SupportedModulesActivity;
import godau.fynn.moodledirect.activity.help.HelpActivity;
import godau.fynn.moodledirect.activity.StorageActivity;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.model.HelpItem;
import godau.fynn.moodledirect.util.FileManagerWrapper;
import godau.fynn.moodledirect.util.ConfigDownloadHelper;
import godau.fynn.moodledirect.util.MyApplication;

import static godau.fynn.moodledirect.util.FileManagerWrapper.REQUEST_OPEN_DIRECTORY;

public class SettingsFragment extends PreferenceFragmentCompat {

    public static final String KEY_SHOW_SETTINGS = "showSettings";
    boolean themeChanged;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if (MyApplication.getInstance().isDarkModeEnabled()) {
            getActivity().setTheme(R.style.AppTheme_NoActionBar_Dark);
        }

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {

        getPreferenceManager().setSharedPreferencesName(
                PreferenceHelper.SETTINGS_FILE
        );

        setPreferencesFromResource(R.xml.preferences, s);

        findPreference("offline").setOnPreferenceChangeListener(((preference, newValue) -> {

            ((MyApplication) requireActivity().getApplication()).networkStateReceiver.onUpdateForce((boolean) newValue);

            return true;
        }));

        CheckBoxPreference darkMode = findPreference("dark_theme");
        darkMode.setOnPreferenceChangeListener((preference, o) -> {

            this.themeChanged = true;

            MyApplication.getInstance().setDarkModeEnabled((Boolean) o);

            getActivity().recreate();

            return true;
        });

        Preference downloadPath = findPreference("download_path");
        updateDownloadPathSummary();
        downloadPath.setOnPreferenceClickListener(preference -> {
            startActivityForResult(FileManagerWrapper.requestPermissionIntent(), REQUEST_OPEN_DIRECTORY);
            return true;
        });

        Preference manageStorage = findPreference("manage_storage");
        manageStorage.setOnPreferenceClickListener(preference -> {
            Intent intent = new Intent(requireActivity(), StorageActivity.class);
            intent.putExtra(StorageActivity.EXTRA_UP_FINISHES, true);
            startActivity(intent);
            return true;
        });

        CheckBoxPreference webAutoLogin = findPreference("web_auto_login");
        if (new PreferenceHelper(getContext()).getUserAccount().hasPrivateToken()) {
            // Update cooldown interval
            webAutoLogin.setOnPreferenceChangeListener((preference, newValue) -> {
                if (((Boolean) newValue)) {
                    ConfigDownloadHelper.updateAutoLoginCooldown(requireContext());
                }
                return true;
            });
        } else {
            webAutoLogin.setChecked(false);
            webAutoLogin.setSummary(R.string.settings_web_auto_login_disabled_summary);
            webAutoLogin.setOnPreferenceClickListener(preference -> {
                Intent helpIntent = new Intent(requireActivity(), HelpActivity.class);
                helpIntent.putExtra(HelpActivity.EXTRA_HELP_ITEM, HelpItem.autoLogin);
                startActivity(helpIntent);
                return true;
            });
            // Reject changing the preference
            webAutoLogin.setOnPreferenceChangeListener((preference, value) -> false);
        }

        Preference supportedModulesPreference = findPreference("supported_modules");
        supportedModulesPreference.setOnPreferenceClickListener(preference -> {
            startActivity(new Intent(requireContext(), SupportedModulesActivity.class));
            return true;
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState){

        if (themeChanged) outState.putBoolean(KEY_SHOW_SETTINGS, true);

        super.onSaveInstanceState(outState);
    }

    private void updateDownloadPathSummary() {
        SharedPreferences preferences = getPreferenceManager().getSharedPreferences();

        Preference downloadPath = findPreference("download_path");
        if (!preferences.contains("download_path")) {
            downloadPath.setSummary(R.string.settings_storage_location_unset);
        } else {
            // Test availability of storage location
            DocumentFile location = DocumentFile.fromTreeUri(requireContext(), Uri.parse(preferences.getString("download_path", null)));
            if (location != null && location.exists()) {
                downloadPath.setSummary(R.string.settings_storage_location_change);
            } else {
                downloadPath.setSummary(R.string.settings_storage_location_unavailable);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_OPEN_DIRECTORY && resultCode == Activity.RESULT_OK) {
            FileManagerWrapper.onRequestPermissionResult(data, requireContext());
            updateDownloadPathSummary();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}

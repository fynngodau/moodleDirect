package godau.fynn.moodledirect.activity.login.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.login.LoginActivity;
import godau.fynn.moodledirect.module.basic.Login;
import godau.fynn.moodledirect.network.exception.NotOkayException;
import godau.fynn.moodledirect.util.Constants;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.ScrollableFragment;
import okhttp3.HttpUrl;

public class ServerFragment extends ScrollableFragment {

    private Drawable openLockDrawable;
    private boolean closedLockDisplayed = false;

    private boolean submitAllowed = true;
    private EditText urlEditText;
    /**
     * URL to be filled due to {@link #fillUrl(String)}
     */
    private String fillUrl = "";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ScrollView container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_server, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        urlEditText = view.findViewById(R.id.url);
        Button checkUrl = view.findViewById(R.id.connect);

        requireActivity().setTitle(getString(R.string.login_title));

        urlEditText.setOnKeyListener((v, i, keyEvent) -> {
                    if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)) {
                        checkUrl.performClick();
                        return true;
                    } else return false;
                }
        );

        urlEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().toLowerCase().startsWith("http:")) {

                    if (openLockDrawable == null) {
                        openLockDrawable = AppCompatResources.getDrawable(requireContext(), R.drawable.ic_lock_open);
                        openLockDrawable.setBounds(0, 0,
                                openLockDrawable.getIntrinsicWidth(), openLockDrawable.getIntrinsicHeight());
                    }

                    urlEditText.setError(getString(R.string.login_warning_insecure), openLockDrawable);
                    closedLockDisplayed = false;
                } else if (s.toString().isEmpty()) {
                    urlEditText.setCompoundDrawables(null, null, null, null);
                    closedLockDisplayed = false;
                } else {
                    if (!closedLockDisplayed) {
                        urlEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                        closedLockDisplayed = true;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        urlEditText.setText(fillUrl);

        urlEditText.requestFocus();

        checkUrl.setOnClickListener(v -> {
            if (!submitAllowed) return;

            String url = urlEditText.getText().toString();
            if (url.length() < 1) {
                urlEditText.setError(getString(R.string.login_error_no_url));
                closedLockDisplayed = false;
                return;
            }

            if (!url.startsWith("http")) {
                url = "https://" + url;
            }

            if (HttpUrl.parse(url) == null) {
                urlEditText.setError(getString(R.string.login_error_invalid_url));
                closedLockDisplayed = false;
                return;
            }

            String finalUrl = url;
            submitAllowed = false;

            ProgressDialog progressDialog = new ProgressDialog(requireContext(), R.style.LoginTheme_AlertDialog);
            progressDialog.setMessage(requireContext().getString(R.string.login_progress_connecting));
            progressDialog.setCancelable(false);
            progressDialog.show();

            Login login = new Login(requireContext());
            ExceptionHandler.tryAndThenThread(
                    () -> login.getPublicConfig(finalUrl),
                    response -> {
                        progressDialog.dismiss();
                        submitAllowed = true;

                        Constants.API_URL = response.getHttpswwwroot();

                        boolean authInBrowser = response.typeoflogin != 1;


                        requireActivity().setTitle(getString(R.string.login_title_name, response.sitename));
                        ((LoginActivity) requireActivity()).setPublicConfig(response);

                        if (authInBrowser && requireActivity().getCurrentFocus() != null) {
                            InputMethodManager manager = (InputMethodManager) requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            manager.hideSoftInputFromWindow(requireActivity().getCurrentFocus().getWindowToken(), 0);
                        }

                        Fragment nextFragment;
                        if (authInBrowser) {
                            nextFragment = BrowserFragment.get(response);
                        } else {
                            nextFragment = CredentialsFragment.get(response);
                        }

                        // Transition to next fragment through fragment manager
                        requireActivity()
                                .getSupportFragmentManager().beginTransaction()
                                .setReorderingAllowed(true)
                                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                                        R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.fragment_container, nextFragment)
                                .addToBackStack(null)
                                .commit();

                    },
                    failure -> {
                        progressDialog.dismiss();
                        submitAllowed = true;

                        if (failure instanceof NotOkayException) {
                            // Server responded, but did not behave like a Moodle instance would
                            urlEditText.setError(getString(R.string.login_error_no_moodle));
                            ((LoginActivity) requireActivity()).getAlertDialog(R.string.login_error_no_moodle_detail)
                                    .show();
                        } else {
                            urlEditText.setError(getString(R.string.login_error_no_connection));
                        }
                        closedLockDisplayed = false;
                    },
                    requireContext()
            );
        });
    }

    /**
     * Fills the edit text with the provided URL.
     */
    public void fillUrl(String url) {
        if (urlEditText == null) {
            fillUrl = url;
        } else urlEditText.setText(url);
    }
}

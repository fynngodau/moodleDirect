package godau.fynn.moodledirect.activity.fragment;

import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.CourseDetailActivity;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.util.TextUtil;
import godau.fynn.moodledirect.view.ImageLoaderTextView;

import java.util.Collections;

public class CourseInformationFragment extends Fragment {

    private static final String KEY_COURSE_ID = "id"; // COURSE on T9

    private ImageLoaderTextView description;
    private TextView participants, duration;

    public static CourseInformationFragment newInstance(int courseId) {
        CourseInformationFragment fragment = new CourseInformationFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(KEY_COURSE_ID, courseId);
        fragment.setArguments(arguments);
        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_course_information, container, false);

        description = layout.findViewById(R.id.description);
        participants = layout.findViewById(R.id.participants);
        duration = layout.findViewById(R.id.duration);

        return layout;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        int courseId = getArguments().getInt(KEY_COURSE_ID);

        ExceptionHandler.tryAndThenThread(
                () -> MyApplication.moodle().forceOffline().getCore().getCourse(courseId),
                course -> {

                    // Set course description
                    if (course.summary.trim().isEmpty()) {
                        description.setVisibility(View.GONE);
                    } else {
                        description.setVisibility(View.VISIBLE);

                        SpannableStringBuilder renderedText = TextUtil.fromHtml(course.summary, requireContext(), description.getWidth());
                        description.setText(renderedText,
                                Collections.emptyList(), MyApplication.moodle().getDispatch().getCommonAsset(),
                                getResources().getDisplayMetrics()
                        );
                        description.setTextIsSelectable(true);
                        description.setMovementMethod(LinkMovementMethod.getInstance());
                    }

                    participants.setText(getString(R.string.course_participants, course.enrolledUserCount));

                    // Render duration string

                    String start;
                    if (course.timeStart * 1000 > System.currentTimeMillis()) { // start is in future
                        start = getString(R.string.course_duration_start_future,
                                DateUtils.getRelativeTimeSpanString(requireContext(), course.timeStart * 1000, true)
                        );
                    } else {
                        start = getString(R.string.course_duration_start_past,
                                DateUtils.getRelativeTimeSpanString(requireContext(), course.timeStart * 1000, true)
                        );
                    }

                    String end = null;
                    if (course.timeEnd > 0 && course.timeEnd * 1000 > System.currentTimeMillis()) { // end is in future
                        end = getString(R.string.course_duration_end_future,
                                DateUtils.getRelativeTimeSpanString(requireContext(), course.timeEnd * 1000, true)
                        );
                    } else if (course.timeEnd > 0) { // end is set and already in the past
                        end = getString(R.string.course_duration_end_past,
                                DateUtils.getRelativeTimeSpanString(requireContext(), course.timeEnd * 1000, true)
                        );
                    }

                    duration.setText(TextUtil.combine(", ", start, end));
                },
                getContext()
        );
    }
}

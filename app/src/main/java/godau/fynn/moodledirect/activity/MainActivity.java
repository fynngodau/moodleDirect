package godau.fynn.moodledirect.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.transition.AutoTransition;
import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.*;
import godau.fynn.moodledirect.activity.help.HelpActivity;
import godau.fynn.moodledirect.activity.login.LoginActivity;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.data.persistence.UserAccount;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.network.NetworkStateReceiver;
import godau.fynn.moodledirect.util.Constants;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.util.UserUtils;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, NetworkStateReceiver.OfflineStatusChangeListener {

    private UserAccount userAccount;

    private TextView offlineIndicator;
    private ViewGroup root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (MyApplication.getInstance().isDarkModeEnabled()) {
            setTheme(R.style.AppTheme_NoActionBar_Dark_TransparentStatus);
        }

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
          | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        super.onCreate(savedInstanceState);

        if (!new PreferenceHelper(this).isLoggedIn()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);

            finish();
            return;
        }

        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        offlineIndicator = findViewById(R.id.offlineIndicator);
        root = findViewById(R.id.drawer_layout);
        if (root == null) { // for width > 600dp
            root = findViewById(R.id.root);

            root.setOnApplyWindowInsetsListener(((v, insets) -> {
                findViewById(R.id.app_bar_app_layout).setPadding(
                  0, insets.getSystemWindowInsetTop(), 0, 0
                );
                return insets;
            }));
        }

        userAccount = new PreferenceHelper(this).getUserAccount();
        Constants.TOKEN = userAccount.getToken();

        setTitle(userAccount.getMoodleInstanceName());

        if (root instanceof DrawerLayout) {
            DrawerLayout drawer = (DrawerLayout) root;

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
            };

            drawer.setDrawerListener(toggle);
            toggle.syncState();
        }

        // Set up the nav panel
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        TextView username = headerView.findViewById(R.id.username);
        TextView fullName = headerView.findViewById(R.id.firstname);

        username.setText(userAccount.getUserLoginName());
        fullName.setText(userAccount.getUserFirstName());

        headerView.setOnApplyWindowInsetsListener(((v, insets) -> {
            headerView.findViewById(R.id.frame).setPadding(0, insets.getSystemWindowInsetTop(), 0, 0);
            return insets;
        }));

        // Load avatar, don't attempt to use network if offline
        RequestCreator creator = Picasso.get().load(
                userAccount.getAvatarUrl()
                        .replace("/webservice/pluginfile.php/", "/pluginfile.php/")
                        .replace("/pluginfile.php/",
                                "/tokenpluginfile.php/" + userAccount.getFilePrivateAccessKey() + "/"
                        )
        );
        if (NetworkStateReceiver.getOfflineStatus()) {
            creator.networkPolicy(NetworkPolicy.OFFLINE);
        }
        creator.into((ImageView) headerView.findViewById(R.id.user_avatar));

        // Set up fragments
        if (savedInstanceState == null) {
            pushView(new EnrolledCoursesFragment(), "My Courses", true);
        }

        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment frag = getSupportFragmentManager().findFragmentById(R.id.content_main);
            if (frag instanceof EnrolledCoursesFragment){
                navigationView.setCheckedItem(R.id.my_courses);
            }
        });

        resolveModuleLinkShare();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ((MyApplication) getApplication()).networkStateReceiver.addListener(this);
    }

    @Override
    public void onNewOfflineStatus(boolean offline) {
        if (offline) {
            TransitionManager.beginDelayedTransition(root, new ChangeBounds());
            offlineIndicator.setVisibility(View.VISIBLE);
        } else {
            TransitionManager.beginDelayedTransition(root, new AutoTransition());
            offlineIndicator.setVisibility(View.GONE);
        }
    }

    @Deprecated // currently not supported
    private void resolveModuleLinkShare() {
        Intent intent = getIntent();
        String action = intent.getAction();
        Uri uri = intent.getData();
        if(uri != null && action != null && action.equals("android.intent.action.VIEW")) {
            List<Course> courses = new ArrayList<>();//realm.copyFromRealm(realm.where(Course.class).findAll());
            int courseId = Integer.parseInt(uri.getQueryParameter("courseId"));

            boolean isEnrolled = false;
            for(Course course : courses) {
                if(course.getCourseId() == courseId) {
                    isEnrolled = true;
                    break;
                }
            }

            if(isEnrolled) {
                String fileUrl = uri.getScheme() + "://" + uri.getHost() + uri.getPath().replace("/fileShare", "") + "?forcedownload=1&token=" + userAccount.getToken();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(fileUrl));
                startActivity(browserIntent);
            }
            else {
                Toast.makeText(this, "You need to be enrolled in " + uri.getQueryParameter("courseName") + " in order to view", Toast.LENGTH_LONG).show();
            }

        }
    }

    private void pushView(Fragment fragment, String tag, boolean rootFrag){
        if (rootFrag) {
            clearBackStack();
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_main, fragment, tag);
        if (!rootFrag) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }


    private AlertDialog askToLogout() {
        AlertDialog.Builder alertDialog;

        alertDialog = new AlertDialog.Builder(MainActivity.this, R.style.LoginTheme_AlertDialog);

        alertDialog.setMessage(R.string.confirm_logout);
        alertDialog.setPositiveButton(R.string._yes, (dialogInterface, i) -> logout());
        alertDialog.setNegativeButton(R.string._cancel, null);
        return alertDialog.create();
    }

    public void logout() {
        UserUtils.logout(this);
        finish();
        startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();
        switch (id) {
            case R.id.my_courses:
                pushView(new EnrolledCoursesFragment(), getString(R.string.my_courses), true);
                break;

            case R.id.course_search:
                pushView(new SearchCourseFragment(), getString(R.string.browse_courses), false);
                break;

            case R.id.calendar:
                pushView(new CalendarFragment(), getString(R.string.calendar_title), false);
                break;

            case R.id.settings:
                pushView(new SettingsFragment(), getString(R.string.settings_title), false);
                break;

            case R.id.help:
                Intent helpIntent = new Intent(this, HelpActivity.class);
                startActivity(helpIntent);
                break;

            case R.id.logout:
                askToLogout().show();
                break;

            case R.id.debug:
                pushView(new DebugFragment(), "Debug", false);
                break;
        }
        ViewGroup root = findViewById(R.id.drawer_layout);
        if (root instanceof DrawerLayout) {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    private void clearBackStack() {
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
            getSupportFragmentManager().popBackStack();
        }
    }
}

package godau.fynn.moodledirect.activity.fragment.module;

import android.os.Bundle;
import android.view.*;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.CourseInformationFragment;
import godau.fynn.moodledirect.activity.fragment.SwipeRefreshFragment;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.util.AutoLoginHelper;
import godau.fynn.moodledirect.util.Constants;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.TextUtil;
import godau.fynn.moodledirect.view.ImageLoaderTextView;

public class AssignmentFragment extends SwipeRefreshFragment {

    public static final String KEY_ASSIGNMENT_ID = "assignment";
    public static final String KEY_ASSIGNMENT_NAME = "name";
    public static final String KEY_ASSIGNMENT_URL = "url";

    private int assignmentId;
    private String moduleName;
    private String url;

    private TextView textView, title, grade;

    public static AssignmentFragment newInstance(Module module) {
        AssignmentFragment fragment = new AssignmentFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_ASSIGNMENT_ID, module.instance);
        args.putString(KEY_ASSIGNMENT_NAME, module.getName());
        args.putString(KEY_ASSIGNMENT_URL, module.url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        assignmentId = getArguments().getInt(KEY_ASSIGNMENT_ID);
        moduleName = getArguments().getString(KEY_ASSIGNMENT_NAME);
        url = getArguments().getString(KEY_ASSIGNMENT_URL);

        setHasOptionsMenu(true);
    }

    @Override
    protected View onCreateContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_assignment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textView = view.findViewById(R.id.text);

        title = view.findViewById(R.id.title);
        title.setText(moduleName);

        grade = view.findViewById(R.id.grade);
    }

    @Override
    protected void loadData(MoodleDatabase.Dispatch dispatch) {
        ExceptionHandler.tryAndThenThread(
                () -> dispatch.getAssignment().getSubmissionStatus(assignmentId),
                submission -> {

                    swipeRefreshLayout.setRefreshing(false);

                    if (submission.lastAttempt != null
                            && submission.lastAttempt.submission != null
                            && "submitted".equals(submission.lastAttempt.submission.status)
                            && submission.lastAttempt.getGradingStatus() != null) {
                        textView.setText(submission.lastAttempt.getGradingStatus().string);
                    } else {
                        textView.setText(R.string.assignment_submission_none);
                    }

                    if (submission.feedback != null && submission.feedback.gradeText != null) {
                        grade.setText(
                                TextUtil.fromHtml(
                                        submission.feedback.gradeText,
                                        grade.getContext(),
                                        title.getWidth()
                                )
                        );
                        grade.setVisibility(View.VISIBLE);
                    } else {
                        grade.setVisibility(View.GONE);
                    }
                },
                alsoOnFailure -> swipeRefreshLayout.setRefreshing(false),
                requireContext()
        );
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.open_in_browser, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_open_in_browser) {
            AutoLoginHelper.openWithAutoLogin(requireContext(), getView(), url);
            return true;
        } else return super.onOptionsItemSelected(item);
    }
}

package godau.fynn.moodledirect.activity.fragment.module.choice;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.util.TimeUtils;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.SwipeRefreshFragment;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.model.api.choice.ChoiceOption;
import godau.fynn.moodledirect.module.Choice;
import godau.fynn.moodledirect.network.APIClient;
import godau.fynn.moodledirect.network.MoodleServices;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.util.TextUtil;
import godau.fynn.moodledirect.view.ImageLoaderTextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class ChoiceChooseFragment extends SwipeRefreshFragment {
    private int choiceId;
    private MoodleServices moodleServices;
    private int courseId;
    private boolean allowMultiple;

    private final State state = new State();
    private Button submit;
    private TextView titleTextView;
    private RadioGroup radioGroup;
    private LinearLayout checkGroup;
    private ImageLoaderTextView descriptionTextView;
    private TextView noChangeAllowed;
    private Button undo;
    private ViewGroup restrictionLayout;
    private TextView restriction;
    private DisplayMetrics metrics;

    private Choice.ChoiceObject choice = null;

    private Context context;

    ArrayList<Integer> checked = new ArrayList<>();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        choiceId = getArguments().getInt(ChoiceFragmentFactory.EXTRA_CHOICE_ID);
        courseId = getArguments().getInt(ChoiceFragmentFactory.EXTRA_COURSE_ID);
        if (moodleServices == null) {
            moodleServices = APIClient.getServices();
        }
    }

    @Nullable
    @Override
    public View onCreateContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_choice_choose, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        radioGroup = view.findViewById(R.id.radioGroup);
        checkGroup = view.findViewById(R.id.checkGroup);
        titleTextView = view.findViewById(R.id.title);
        descriptionTextView = view.findViewById(R.id.description);

        noChangeAllowed = view.findViewById(R.id.no_change_allowed);
        undo = view.findViewById(R.id.undo);

        submit = view.findViewById(R.id.submit);

        restrictionLayout = view.findViewById(R.id.restrictionLayout);
        restriction = view.findViewById(R.id.restriction);

        // DisplayMetrics for scaling images
        metrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        submit.setOnClickListener((v) -> {

            if (checked.size() == 0) {
                Toast.makeText(getContext(), R.string.choice_submit_error_nothing_chosen, Toast.LENGTH_SHORT).show();
                return;
            }

            state.networkInProgress = true;
            pushState(state);

            ExceptionHandler.tryAndThenThread(
                    () -> MyApplication.moodle().getDispatch().getChoice().setChosenOptions(choiceId, checked),
                    onSuccess -> {
                        state.submitted = true;
                        pushState(state);

                        Toast.makeText(getContext(), R.string.choice_submit_success, Toast.LENGTH_SHORT).show();

                        loadData(MyApplication.moodle().getDispatch());
                        ((ChoiceFragment) getParentFragment()).notifyResultsChanged();
                    },
                    this::onNetworkError,
                    context
            );
        });

        undo.setOnClickListener((v) -> {

            state.networkInProgress = true;
            pushState(state);

            ExceptionHandler.tryAndThenThread(
                    () -> MyApplication.moodle().getDispatch().getChoice().undoChoice(choiceId),
                    onSuccess -> {

                        state.submitted = false;
                        pushState(state);

                        for (int i = 0; i < checkGroup.getChildCount(); i++) {
                            View childView = checkGroup.getChildAt(i);
                            if (childView instanceof CheckBox) ((CheckBox) childView).setChecked(false);
                        }

                        radioGroup.clearCheck();
                        checked.clear();

                        Toast.makeText(context, R.string.choice_submit_success, Toast.LENGTH_SHORT).show();

                        loadData(MyApplication.moodle().getDispatch());
                        ((ChoiceFragment) getParentFragment()).notifyResultsChanged();
                    },
                    this::onNetworkError,
                    context
            );
        });

        pushState(state);
    }

    @Override
    protected void loadData(MoodleDatabase.Dispatch dispatch) {

        state.networkInProgress = true;
        pushState(state);

        ExceptionHandler.tryAndThenThread(
                // Ignore given dispatch; choice should be updated even if restoring instance state
                () -> ((ChoiceFragment) getParentFragment()).getChoiceObject(),
                choice -> {
                    state.networkInProgress = false;
                    this.choice = choice;

                    // Clear previous
                    radioGroup.removeAllViews();
                    checkGroup.removeAllViews();
                    checked.clear();

                    titleTextView.setText(choice.details.name);

                    if (choice.details.description.isEmpty())
                        descriptionTextView.setVisibility(View.GONE);
                    else {
                        descriptionTextView.setText(
                                TextUtil.fromHtml(choice.details.description, descriptionTextView.getContext(), descriptionTextView.getWidth()),
                                choice.details.descriptionFiles.stream().map(file -> file.url).collect(Collectors.toList()),
                                MyApplication.moodle().getDispatch().getCommonAsset(),
                                metrics
                        );
                    }


                    ViewGroup group;
                    if (allowMultiple = choice.details.allowMultiple) {
                        radioGroup.setVisibility(View.GONE);
                        group = checkGroup;
                    } else {
                        radioGroup.setVisibility(View.VISIBLE);
                        group = radioGroup;
                    }

                    state.choicesAvailable = choice.options.size() > 0;
                    for (ChoiceOption option : choice.options) {

                        // Generate text spannable
                        SpannableStringBuilder text = new SpannableStringBuilder(option.text);
                        if (!option.disabled) {
                            text.setSpan(TextUtil.createColorAttributeSpan(
                                    context, android.R.attr.textColorTertiary
                            ), text.length(), text.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        }
                        if (choice.details.limitAnswers) {
                            int remaining = option.limit - option.answerCount;
                            text.append('\t');
                            if (remaining > 0) {
                                text.append(getString(R.string.choice_limit, remaining));
                            } else if (remaining == 0) {
                                text.append(getString(R.string.choice_limit_reached));
                            } else { // if (remaining < 0)
                                text.append(getString(R.string.choice_limit_exceeded, -remaining));
                            }
                        }

                        View selectableView = createSelectableView(
                                text, option.id, option.checked, !option.disabled
                        );
                        group.addView(selectableView);
                        if (option.checked)
                            ChoiceChooseFragment.this.checked.add(option.id);
                    }

                    state.submitted = false;
                    for (ChoiceOption option : choice.options) {
                        if (option.checked) {
                            state.submitted = true;
                            break;
                        }
                    }

                    // Show restriction
                    boolean choiceNotYetOpen = (choice.details.timeOpen > 0
                            && choice.details.timeOpen > (System.currentTimeMillis() / 1000));
                    boolean choiceCloses = choice.details.timeClose > 0;
                    boolean choiceClosed = choice.details.timeClose <= (System.currentTimeMillis() / 1000);

                    if (choiceNotYetOpen | choiceCloses) {
                        restrictionLayout.setVisibility(View.VISIBLE);

                        String text;
                        if (choiceNotYetOpen & choiceCloses) {
                            // Choice will open and close
                            text = context.getString(
                                    R.string.choice_availability_not_yet_will_close,
                                    DateUtils.getRelativeTimeSpanString(context, choice.details.timeOpen * 1000, true),
                                    DateUtils.getRelativeTimeSpanString(context, choice.details.timeClose * 1000, true)
                            );
                        } else if (choiceNotYetOpen & !choiceCloses) {
                            // Choice will open but never close
                            text = context.getString(
                                    R.string.choice_availability_not_yet,
                                    DateUtils.getRelativeTimeSpanString(context, choice.details.timeOpen * 1000, true)
                            );
                        } else if (!choiceClosed) { // if (!choiceNotYetOpen & choiceCloses & !choiceClosed)
                            // Choice is going to close
                            text = context.getString(
                                    R.string.choice_availability_will_close,
                                    DateUtils.getRelativeTimeSpanString(context, choice.details.timeClose * 1000, true)
                            );
                        } else { // if (!choiceNotYetOpen & choiceCloses & choiceClosed)
                            // Choice has already closed
                            text = context.getString(
                                    R.string.choice_availability_has_closed,
                                    DateUtils.getRelativeTimeSpanString(context, choice.details.timeClose * 1000, true)
                            );
                        }
                        restriction.setText(text);
                    }

                    state.allowUpdate = choice.details.allowUpdate;

                    empty.hide();
                    pushState(state);
                },
                e -> {
                    if (choice == null) {
                        empty.exception(e);
                    }
                    onNetworkError(e);
                },
                context
        );
    }

    private CompoundButton createSelectableView(CharSequence text, int id, boolean checked, boolean enabled) {

        CompoundButton.OnCheckedChangeListener listener = (buttonView, isChecked) -> {
            if (isChecked) {
                this.checked.add(id);

                for (int i = 0; i < radioGroup.getChildCount(); i++) {
                    RadioButton button = (RadioButton) radioGroup.getChildAt(i);
                    if (!button.equals(buttonView))
                        button.setChecked(false);
                }
            } else
                this.checked.remove((Integer) id);
        };

        if (allowMultiple) {
            CheckBox checkBox = new CheckBox(getContext());
            checkBox.setText(text);
            checkBox.setEnabled(enabled);
            checkBox.setChecked(checked);

            checkBox.setOnCheckedChangeListener(listener);

            return checkBox;
        } else {
            RadioButton radioButton = new RadioButton(getContext());
            radioButton.setText(text);
            radioButton.setEnabled(enabled);
            radioButton.setChecked(checked);

            radioButton.setOnCheckedChangeListener(listener);

            return radioButton;
        }
    }

    private void pushState(State state) {

        if (radioGroup == null) {
            // Views have not yet been created – wait
            return;
        }

        // Render concrete view state based on model state
        boolean undoVisible, undoEnabled, undoBright, // bright = not grayed out
                submitVisible, submitEnabled, submitBright,
                optionsEnabled,
                noChangeAllowedVisible,
                swipeRefreshLayoutRefreshing;
        @StringRes int noChangeAllowedText;

        undoVisible = state.submitted & state.allowUpdate;
        undoBright = !state.networkInProgress;
        undoEnabled = undoBright;

        submitVisible = state.choicesAvailable;
        submitBright = !(!state.allowUpdate & (state.submitted | state.networkInProgress));
        submitEnabled = submitBright & !state.networkInProgress;

        optionsEnabled = submitBright;

        noChangeAllowedVisible = !state.allowUpdate && state.choicesAvailable;
        noChangeAllowedText = state.submitted? R.string.choice_submitted_no_change_allowed : R.string.choice_no_change_allowed;

        swipeRefreshLayoutRefreshing = state.networkInProgress;

        // Apply this state to the given views

        for (int i = 0; i < checkGroup.getChildCount(); i++) {
            checkGroup.getChildAt(i).setEnabled(!choice.options.get(i).disabled & optionsEnabled);
        }

        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            radioGroup.getChildAt(i).setEnabled(!choice.options.get(i).disabled & optionsEnabled);
        }

        undo.setVisibility(undoVisible? View.VISIBLE : View.GONE);
        undo.setEnabled(undoEnabled);
        undo.setAlpha(undoBright? 1f : 0.5f);

        submit.setVisibility(submitVisible? View.VISIBLE : View.GONE);
        submit.setEnabled(submitEnabled);
        submit.setAlpha(submitBright? 1f : 0.5f);

        noChangeAllowed.setVisibility(noChangeAllowedVisible? View.VISIBLE : View.GONE);
        if (noChangeAllowedVisible) {
            noChangeAllowed.setText(noChangeAllowedText);
        }

        swipeRefreshLayout.setRefreshing(swipeRefreshLayoutRefreshing);
    }

    private void onNetworkError(Throwable t) {
        state.networkInProgress = false;
        pushState(state);
    }

    private static class State {
        boolean allowUpdate = true;
        boolean submitted = false;
        boolean networkInProgress = true;
        boolean choicesAvailable = false;
    }
}

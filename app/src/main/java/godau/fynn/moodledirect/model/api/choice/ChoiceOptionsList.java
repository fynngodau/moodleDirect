package godau.fynn.moodledirect.model.api.choice;

import java.util.List;

/**
 * Only for {@code Choice.getChoiceOptions}!
 */
public class ChoiceOptionsList {
    public List<ChoiceOption> options;
}

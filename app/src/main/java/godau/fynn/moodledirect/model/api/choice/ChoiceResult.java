package godau.fynn.moodledirect.model.api.choice;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

@Entity(tableName = "choiceResult")
public class ChoiceResult {
    @PrimaryKey
    public int id;
    public String text;

    /**
     * Amount of users who have selected this option.
     */
    @SerializedName("numberofuser")
    public int answerCount;

    /**
     * Amount of "slots", i.e. the maximum count of users that may pick this option.
     */
    @SerializedName("maxanswer")
    public int answerLimit;

    /**
     * Percentage value, i.e. within [0;100] (not [0;1]!), of users who have selected
     * this option.
     */
    @SerializedName("percentageamount")
    public double answerPercentage;

    @SerializedName("userresponses")
    @Ignore // private data of other users should not be stored in our local database
    public List<UserResponse> users = Collections.emptyList();

    // === DATABASE ===
    public int reference;
    public int orderNumber;
}

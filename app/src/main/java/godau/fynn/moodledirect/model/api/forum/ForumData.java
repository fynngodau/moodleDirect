package godau.fynn.moodledirect.model.api.forum;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by siddhant on 1/17/17.
 */

public class ForumData {

    @SerializedName(value = "discussions", alternate = {"posts"})
    public List<Discussion> discussions = null;
    public List<Object> warnings = null;

}

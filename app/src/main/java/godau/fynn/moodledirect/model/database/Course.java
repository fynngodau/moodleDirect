package godau.fynn.moodledirect.model.database;

import android.text.Html;

import androidx.annotation.Nullable;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.RoomWarnings;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import godau.fynn.moodledirect.model.api.file.File;

/**
 * Created by harsu on 16-12-2016.
 */
@Entity(tableName = "course")
@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
public class Course implements Serializable {

    @PrimaryKey
    public final int id;
    @SerializedName("enrolledusercount")
    public int enrolledUserCount;
    public String shortname, summary, format;
    @SerializedName("fullname")
    public String name;

    /**
     * Seconds since epoch at which this course has begun.
     */
    @SerializedName("startdate")
    public long timeStart;

    /**
     * Seconds since epoch at which this course will end, or 0 if unset.
     */
    @SerializedName("enddate")
    public long timeEnd;

    @Embedded(prefix = "header_") @Nullable
    public File headerImage;

    @Nullable
    public String topCategoryName;

    @Nullable @SerializedName("categoryname")
    public String categoryName;

    public Course(int id, int enrolledUserCount, String shortname, String name, String summary, String format) {
        this.id = id;
        this.enrolledUserCount = enrolledUserCount;
        this.shortname = shortname;
        this.name = name;
        this.summary = summary;
        this.format = format;
    }

    // === DOWNLOAD ===

    @Ignore
    @SerializedName(value = "category", alternate = {"categoryid"})
    public int categoryId;

    @Ignore @Nullable
    List<File> overviewfiles;

    /**
     * To be called exactly once after the Course was deserialized.
     */
    public void prepareAfterDownload() {
        // Resolve HTML characters like &amp;
        shortname = Html.fromHtml(shortname).toString();
        name = Html.fromHtml(name).toString();

        // Make header
        if (overviewfiles == null) return;
        // Find image
        for (File file : overviewfiles) {
            if (file.mimetype.startsWith("image/")) {
                headerImage = file;
                return;
            }
        }
    }

    // === LEGACY ===

    @Ignore
    public int downloadStatus;
    @Ignore
    public int totalFiles;
    @Ignore
    public int downloadedFiles;

    public void setDownloadStatus(int downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public int getId() {
        return id;
    }

    public int getCourseId() {
        return id;
    }

    // === DEFAULT ===

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Course && ((Course) obj).getId() == id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}

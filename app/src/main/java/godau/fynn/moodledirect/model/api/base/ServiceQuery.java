package godau.fynn.moodledirect.model.api.base;

import java.util.List;

public class ServiceQuery {
    public int index;
    public String methodname;
    public Args args;

    public ServiceQuery(int index, String methodname, Args args) {
        this.index = index;
        this.methodname = methodname;
        this.args = args;
    }

    public static class Args {
        public String qrloginkey, userid;

        public Args() {
        }

        public Args(String qrloginkey, String userid) {
            this.qrloginkey = qrloginkey;
            this.userid = userid;
        }
    }
}

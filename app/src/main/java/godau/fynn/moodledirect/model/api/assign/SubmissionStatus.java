package godau.fynn.moodledirect.model.api.assign;

import com.google.gson.annotations.SerializedName;

public class SubmissionStatus {

    public SubmissionFeedback feedback;

    @SerializedName("lastattempt")
    public Attempt lastAttempt;
}

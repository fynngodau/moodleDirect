package godau.fynn.moodledirect.model.api.enrol;

import com.google.gson.annotations.SerializedName;

public class GuestEnrolOption extends EnrolmentOption {
    @SerializedName("passwordrequired")
    public boolean requiresPassword;
}

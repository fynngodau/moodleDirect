package godau.fynn.moodledirect.view.adapter.course;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.moodledirect.model.ResourceType;
import godau.fynn.moodledirect.model.database.CourseSection;
import godau.fynn.moodledirect.model.database.Module;

/**
 * @implNote Derived from {@link DividerItemDecoration}
 */
public class ModuleDividerDecoration extends DividerItemDecoration {

    private static final String TAG = "ModuleDividerItem";
    private static final int[] ATTRS = new int[]{ android.R.attr.listDivider };

    private final Drawable divider;
    private final Rect bounds = new Rect();

    private final RecyclerView.LayoutManager layoutManager;
    private final ModuleAdapter adapter;

    /**
     * Creates a divider {@link RecyclerView.ItemDecoration} that can be used with a
     * {@link LinearLayoutManager}.
     *
     * @param context     Current context, it will be used to access resources.
     */
    public ModuleDividerDecoration(Context context, ModuleAdapter adapter, RecyclerView.LayoutManager layoutManager) {
        super(context, VERTICAL);

        this.adapter = adapter;
        this.layoutManager = layoutManager;

        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        divider = a.getDrawable(0);
        if (divider == null) {
            Log.w(TAG, "@android:attr/listDivider was not set in the theme used for this "
                    + "DividerItemDecoration. Please set that attribute or adapt the divider class");
        }
        a.recycle();
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (parent.getLayoutManager() == null || divider == null) {
            return;
        }
        drawVertical(c, parent);
    }

    /**
     * Determines whether the given {@link View} should be decorated with a horizontal divider
     * at the bottom.
     */
    private boolean needsDecorationBelow(View view) {
        int position = layoutManager.getPosition(view);

        if (position == -1) return false;

        // Is module last one?
        boolean last = adapter.getContent().size() <= position + 1;

        // Is module followed by a label or a CourseSection?
        boolean needsDivider = false;
        if (!last) {
            Module module = adapter.getContent().get(position);
            Module next = adapter.getContent().get(position + 1);

            // Show dividers above course sections and labels
            needsDivider = next instanceof CourseSection;
            needsDivider |= next.getModuleType() == ResourceType.LABEL;

            // Show no dividers below course sections and labels (don't separate them from their content)
            needsDivider &= !(
                    module instanceof CourseSection || module.getModuleType() == ResourceType.LABEL
            );
        }

        return needsDivider;

    }

    private void drawVertical(Canvas canvas, RecyclerView parent) {
        canvas.save();
        final int left;
        final int right;

        if (parent.getClipToPadding()) {
            left = parent.getPaddingLeft();
            right = parent.getWidth() - parent.getPaddingRight();
            canvas.clipRect(left, parent.getPaddingTop(), right,
                    parent.getHeight() - parent.getPaddingBottom());
        } else {
            left = 0;
            right = parent.getWidth();
        }

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);

            if (!needsDecorationBelow(child)) continue;

            parent.getDecoratedBoundsWithMargins(child, bounds);
            final int bottom = bounds.bottom + Math.round(child.getTranslationY());
            final int top = bottom - divider.getIntrinsicHeight();
            divider.setBounds(left, top, right, bottom);
            divider.draw(canvas);
        }
        canvas.restore();
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {

        if (needsDecorationBelow(view) && divider != null) {
            outRect.set(0, 0, 0, divider.getIntrinsicHeight());
        } else {
            outRect.set(0, 0, 0, 0);
        }
    }
}

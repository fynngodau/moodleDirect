package godau.fynn.moodledirect.view.adapter;

import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.util.TextUtil;
import godau.fynn.moodledirect.view.ClickListener;
import godau.fynn.typedrecyclerview.SimpleRecyclerViewAdapter;

import java.util.List;

public class CoursesAdapter extends SimpleRecyclerViewAdapter<Course, CoursesAdapter.ViewHolder> {

    private ClickListener<Course> clickListener;

    private final PreferenceHelper.CourseRowAppearance appearance;

    public CoursesAdapter(List<Course> courseList, PreferenceHelper.CourseRowAppearance courseRowAppearance) {
        super(courseList);
        this.appearance = courseRowAppearance;
    }

    public void setClickListener(ClickListener<Course> clickListener) {
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                inflater.inflate(R.layout.row_course, parent, false),
                clickListener, content
        );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, Course item, int position) {

        // Set course name text
        holder.courseName.setText(item.name);

        // Set category text

        String text = null;
        boolean hide = appearance.categoryAppearance.equals("none");

        if (item.categoryName == null) hide = true;
        else if (item.categoryName.equals(item.topCategoryName)) text = item.categoryName;
        else switch (appearance.categoryAppearance) {
            case "bottom":
            default:
                text = item.categoryName;
                break;
            case "top":
                if (item.topCategoryName != null) {
                    text = item.topCategoryName;
                } else {
                    hide = true;
                }
                break;
            case "both":
                if (item.topCategoryName != null) {
                    text = item.topCategoryName + " / " + item.categoryName;
                } else {
                    text = item.categoryName;
                }
                break;
        }

        if (hide) {
            holder.courseCategory.setVisibility(View.GONE);
        } else {
            holder.courseCategory.setVisibility(View.VISIBLE);
            holder.courseCategory.setText(text);
        }

        // Set course summary text
        if (!appearance.showDescription || item.summary.trim().isEmpty()) {
            holder.courseInfo.setVisibility(View.GONE);
        } else {
            CharSequence charSequence = TextUtil.fromHtml(item.summary, context, holder.courseInfo.getWidth());
            if (TextUtil.hasContent(charSequence)) {
                holder.courseInfo.setText(TextUtil.crop(charSequence, 120));
                holder.courseInfo.setVisibility(View.VISIBLE);
            } else {
                holder.courseInfo.setVisibility(View.GONE);
            }
        }

        // TODO Set course unread counter?
        if (appearance.showUnreadCounter) {
        } else {
        }
        //holder.unreadCount.setVisibility(View.GONE);

        // Load overview image
        if (appearance.showHeaderImage && item.headerImage != null) {
            PreferenceHelper preferences = new PreferenceHelper(context);
            File headerFile = item.headerImage;
            holder.imageView.setTag(headerFile);
            holder.imageView.setVisibility(View.VISIBLE);
            holder.imageView.setImageDrawable(null);
            holder.imageView.post(() -> {
                RequestCreator creator =
                        Picasso.get().load(Uri.parse(
                                        headerFile.url + "?token=" + preferences.getUserAccount().getToken()
                                )).fit().centerCrop()
                                .memoryPolicy(MemoryPolicy.NO_STORE);
                if (preferences.isForceOfflineModeEnabled()) {
                    creator.networkPolicy(NetworkPolicy.OFFLINE);
                }
                creator.into(holder.imageView);
            });
        } else {
            holder.imageView.setVisibility(View.GONE);
            holder.imageView.setTag(null);
        }
    }


    public void setCourses(List<Course> courseList) {
        if (courseList != content) {
            content.clear();
            content.addAll(courseList);
        }

        /*for (Course course : content) {
            course.setDownloadStatus(-1);
        }*/

        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView courseName;
        //private final TextView unreadCount;
        private final TextView courseInfo;
        private final TextView courseCategory;
        private final ImageView imageView;

        ViewHolder(View itemView, ClickListener<Course> clickListener, final List<Course> content) {
            super(itemView);
            courseName = itemView.findViewById(R.id.courseName);
            //unreadCount = itemView.findViewById(R.id.unreadCount);
            courseInfo = itemView.findViewById(R.id.courseInfo);
            imageView = itemView.findViewById(R.id.header_image);
            courseCategory = itemView.findViewById(R.id.courseCategory);

            itemView.setOnClickListener(view -> {
                if (clickListener != null) {
                    int pos = getLayoutPosition();
                    clickListener.onClick(content.get(pos), pos);
                }
            });
        }
    }

}

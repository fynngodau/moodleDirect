package godau.fynn.moodledirect.view;

import android.text.SpannableStringBuilder;

import java.util.List;

/**
 * Store variables only needed on the current screen
 */
public class ExpandableTextDisplay {
    public SpannableStringBuilder text;
    public boolean isExpanded;
    public List<String> files;
}

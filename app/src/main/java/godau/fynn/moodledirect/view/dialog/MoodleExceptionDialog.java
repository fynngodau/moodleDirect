package godau.fynn.moodledirect.view.dialog;

import android.app.AlertDialog;
import android.content.Context;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.api.MoodleException;

public class MoodleExceptionDialog extends AlertDialog.Builder {

    private final MoodleException e;

    public MoodleExceptionDialog(MoodleException e, Context context) {
        super(context);
        this.e = e;

        switch (e.getErrorCode()) {
            case UNKNOWN:
            case INVALID_TOKEN:
            case INVALID_PARAMETER:
            case MISSING_PARAMETER:
            case FILE_NOT_FOUND: // TODO better handling
            case ACCESS_EXCEPTION:
                setTitle(R.string.moodle_exception);

                if (e.message == null) {
                    setMessage(context.getString(R.string.moodle_exception_code, e.errorcode));
                } else {
                    setMessage(context.getString(
                            R.string.moodle_exception_message,
                            e.message, e.errorcode
                    ));
                }
                break;

            case SITE_MAINTENANCE:
                setTitle(R.string.moodle_exception_maintenance);
                setMessage(context.getString(
                        R.string.moodle_exception_maintenance_message,
                        e.message, e.errorcode
                ));
                break;

            case SITE_CONFIGURATION_NO_DATABASE_CONNECTION:
                setTitle(R.string.moodle_exception_site_configuration);
                setMessage(R.string.moodle_exception_site_configuration_no_database_connection);
                break;

            case SERVICE_UNAVAILABLE:
            case WEB_SERVICE_NOT_ENABLED:
                setTitle(R.string.moodle_exception_service_unavailable);
                setMessage(R.string.moodle_exception_service_unavailable_message);
                break;

            case INVALID_LOGIN:
                setTitle(R.string.moodle_exception_invalid_login);
                setMessage(R.string.moodle_exception_invalid_login_message);
        }

    }
}

package godau.fynn.moodledirect.view.adapter.course;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.util.FileManagerWrapper;
import godau.fynn.moodledirect.util.TextUtil;

public class RestrictedModuleHandler extends ModuleHandler {
    public RestrictedModuleHandler(FileManagerWrapper fileManager) {
        super(fileManager);
    }

    @Override
    public ModuleViewHolder createViewHolder(@NonNull ViewGroup parent) {
        return new RestrictedModuleViewHolder(
                (ViewGroup) inflater.inflate(R.layout.row_course_module_resource_restricted, parent, false)
        );
    }

    @Override
    public void bindViewHolder(@NonNull ModuleViewHolder holder, Module item, int position) {
        super.bindViewHolder(holder, item, position);

        /* The way TReVA works guarantees that this cast is safe, in fact it would do a very
         * similar cast if we were a descendant of
         * TypeHandler<RestrictedModuleHandler.RestrictedModuleViewHolder, Module>.
        */

        RestrictedModuleViewHolder ourHolder = (RestrictedModuleViewHolder) holder;

        // Show availability info
        if (!item.available && item.notAvailableReason != null) {
            ourHolder.restrictionLayout.setVisibility(View.VISIBLE);
            // TODO: Remove "show more" and "…" items
            ourHolder.restriction.setText(TextUtil.fromHtml(item.notAvailableReason, context));
        } else {
            ourHolder.restrictionLayout.setVisibility(View.GONE);
        }
    }

    static class RestrictedModuleViewHolder extends ModuleViewHolder {

        final ViewGroup restrictionLayout;
        final TextView restriction;


        public RestrictedModuleViewHolder(ViewGroup itemView) {
            super(itemView);

            restriction = itemView.findViewById(R.id.restriction);
            restrictionLayout = itemView.findViewById(R.id.restrictionLayout);
        }
    }
}

package godau.fynn.moodledirect.view;

public interface ClickListener<V> {
    void onClick(V object, int position);
}
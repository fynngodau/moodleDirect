package godau.fynn.moodledirect.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.api.enrol.EnrolmentOption;
import godau.fynn.moodledirect.model.api.enrol.GuestEnrolOption;
import godau.fynn.moodledirect.model.api.enrol.SelfEnrolmentOption;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.MyApplication;

import java.util.List;

public class EnrolmentOptionsViewFactory {

    public static void addEnrolmentOptions(ViewGroup to, List<EnrolmentOption> options, Runnable onEnrol, Runnable onGuestEnrol) {

        Context context = to.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        for (EnrolmentOption option : options) {

            if ("true".equals(option.status) && option instanceof SelfEnrolmentOption) {
                // Is self-enrol with password
                SelfEnrolmentOption selfEnrol = ((SelfEnrolmentOption) option);
                View view = inflater.inflate(R.layout.row_enrol_self_password, to, false);

                TextView name = view.findViewById(R.id.self_enrol_text);
                name.setText(option.name);

                EditText key = view.findViewById(R.id.enrol_key);
                key.setHint(selfEnrol.enrollmentPasswordHint);

                to.addView(view);

                Button enroll = view.findViewById(R.id.course_enrol_enrol_button);
                view.findViewById(R.id.course_enrol_enrol_button).setOnClickListener(v -> {
                    enroll.setEnabled(false);
                    ExceptionHandler.tryAndThenThread(
                            () -> MyApplication.moodle().getDispatch().getEnrolment()
                                    .selfEnrol(option.courseId, option.id, key.getEditableText().toString()),
                            enrolmentResult -> {
                                if (enrolmentResult) { // Success
                                    onEnrol.run();
                                } else {
                                    new AlertDialog.Builder(context)
                                            .setMessage(R.string.course_enrol_unsuccessful)
                                            .show();
                                    enroll.setEnabled(true);
                                }
                            },
                            onFailure -> enroll.setEnabled(false),
                            context
                    );
                });
            } else if (option instanceof GuestEnrolOption) {
                // Is a guest preview access
                GuestEnrolOption guestOption = (GuestEnrolOption) option;
                View view;
                if (guestOption.requiresPassword) {

                    // Lacking webservice support – see https://tracker.moodle.org/browse/MDL-74263

                    view = inflater.inflate(R.layout.row_enrol_guest_password, to, false);

                    TextView name = view.findViewById(R.id.self_enrol_text);
                    name.setText(option.name);
                } else {
                    view = inflater.inflate(R.layout.row_enrol_guest, to, false);

                    TextView name = view.findViewById(R.id.self_enrol_text);
                    name.setText(option.name);

                    view.findViewById(R.id.course_enrol_enrol_button).setOnClickListener(v -> {
                        onGuestEnrol.run();
                    });
                }
                to.addView(view);
            } else if ("true".equals(option.status) && option.wsfunction == null) {
                // Is a self-enrol without password (no wsfunction because no additional data is necessary)
                View view = inflater.inflate(R.layout.row_enrol_self, to, false);

                TextView name = view.findViewById(R.id.self_enrol_text);
                name.setText(option.name);

                Button enroll = view.findViewById(R.id.course_enrol_enrol_button);
                enroll.setOnClickListener(v -> {
                    enroll.setEnabled(false);
                    ExceptionHandler.tryAndThenThread(
                            () -> MyApplication.moodle().getDispatch().getEnrolment()
                                    .selfEnrol(option.courseId, option.id),
                            enrolmentResult -> {
                                if (enrolmentResult) { // Success
                                    onEnrol.run();
                                } else {
                                    new AlertDialog.Builder(context)
                                            .setMessage(R.string.course_enrol_unsuccessful)
                                            .show();
                                    enroll.setEnabled(true);
                                }
                            },
                            onFailure -> enroll.setEnabled(true),
                            context
                    );
                });

                to.addView(view);
            } else {
                // Is not an unavailable option
                View view = inflater.inflate(R.layout.row_enrol_unavailable, to, false);

                TextView name = view.findViewById(R.id.self_enrol_text);
                name.setText(option.name);

                TextView unsupported = view.findViewById(R.id.unsupported_text);
                unsupported.setText(option.status);

                to.addView(view);
            }

        }

    }
}
